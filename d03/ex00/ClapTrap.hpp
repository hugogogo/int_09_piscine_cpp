#ifndef CLAPTRAP_HPP
# define CLAPTRAP_HPP

#include <iostream>
#include <sstream>
#include <string>

class ClapTrap {

public:

	ClapTrap( std::string name = ClapTrap::_dName );
	ClapTrap( ClapTrap const & src );				// copy constructor
	~ClapTrap( void );								// destructor

	ClapTrap &	operator=( ClapTrap const & rhs );	// assignement operator

	void	attack(const std::string & target);
	void	takeDamage(unsigned int amount);
	void	beRepaired(unsigned int amount);

	std::string	getName() const;
	std::string	getClass() const;
	int			getHit() const;
	int			getEnergy() const;
	int			getAttack() const;
	int			getNumber() const;

protected:

	int		getTotalNumber() const;

	std::string 	_name;
	std::string 	_class;
	int				_hit;
	int				_energy;
	int				_attack;
	int				_number;

	void	_increaseNumber();

private:

	void		assignValues(ClapTrap & src);
	static int	_totalNumber;

	static const std::string	_dName;
	static const std::string	_dClass;
	static const int			_dHit;
	static const int			_dEnergy;
	static const int			_dAttack;
	static const int			_dNumber;

};

#endif

