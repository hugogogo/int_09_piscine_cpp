#include "ClapTrap.hpp"

# define B_GRAY "\e[1;30m"
# define B_RED "\e[1;31m"
# define B_GREEN "\e[1;32m"
# define B_YELLOW "\e[1;33m"
# define B_BLUE "\e[1;34m"
# define B_PURPLE "\e[1;35m"
# define B_CYAN "\e[1;36m"
# define B_WHITE "\e[1;37m"

# define RESET "\e[0m"

/*********************************************
 * CONSTRUCTORS
 *********************************************/

ClapTrap::ClapTrap( std::string name ) {
	ClapTrap::_increaseNumber();
	_class = _dClass;
	_name = name;
	_hit = _dHit;
	_energy = _dEnergy;
	_attack = _dAttack;
	_number = getTotalNumber();
	std::cout << _class << " " << _name << "-" << _number << " created\n";
	return;
}

ClapTrap::ClapTrap( ClapTrap const & src ) {
	ClapTrap::_increaseNumber();
	_class = _dClass;
	*this = src;
	_number = getTotalNumber();
	std::cout << _class << " " << _name << "-" << _number << " copied from " << src._class << " " << src._name << "-" << src._number << "\n";
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

ClapTrap::~ClapTrap( void ) {
	std::cout << _class << " " << _name << "-" << _number << " destructed\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

ClapTrap &	ClapTrap::operator=( ClapTrap const & rhs ) {

	if ( this != &rhs )
	{
		_name = rhs.getName();
		_hit = rhs.getHit();
		_energy = rhs.getEnergy();
		_attack = rhs.getAttack();
		_number = rhs.getNumber();
	}

	std::cout << _class << " " << _name << "-" << _number << " assigned\n";
	return *this;

}

/*********************************************
 * ACCESSORS
 *********************************************/

std::string	ClapTrap::getName() const {return _name;}
std::string	ClapTrap::getClass() const {return _class;}
int			ClapTrap::getHit() const {return _hit;}
int			ClapTrap::getEnergy() const {return _energy;}
int			ClapTrap::getAttack() const {return _attack;}
int			ClapTrap::getNumber() const {return _number;}
int			ClapTrap::getTotalNumber() const {return _totalNumber;}
void		ClapTrap::_increaseNumber() {ClapTrap::_totalNumber++;}

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	ClapTrap::attack(const std::string & target) {

	std::ostringstream action;
	std::ostringstream state;

	state << B_CYAN "[" B_GREEN << _class[0] << _number << B_PURPLE "h,e,a" B_CYAN ":" B_BLUE << _hit << "," << _energy << "," << _attack << B_CYAN "->";
	action << _class << " " << _name << "-" << _number;

	if (_energy > 0 && _hit > 0)
	{
		_energy--;
		if (_energy < 0)
			_energy = 0;
		action << " attacked " << target << ", causing " B_YELLOW << _attack << RESET << " points of damage" << '\n';
	}
	else
	{
		_attack = 0;
		if (_energy <= 0)
			action << " cannot attack because " B_RED " is out of energy" RESET"\n";
		else if (_hit <= 0)
			action << " cannot attack because " B_RED " is out of hit" RESET "\n";
	}

	state << B_BLUE << _hit << "," << _energy << "," << _attack << B_CYAN "]" RESET;
	std::cout << state.str() << action.str();
}

void	ClapTrap::takeDamage(unsigned int amount) {

	std::ostringstream action;
	std::ostringstream state;

	state << B_CYAN "[" B_GREEN << _class[0] << _number << B_PURPLE "h,e,a" B_CYAN ":" B_BLUE << _hit << "," << _energy << "," << _attack << B_CYAN "->";
	action << _class << " " << _name << "-" << _number;

	if (_hit > 0)
	{
		_hit -= amount;
		if (_hit < 0)
			_hit = 0;
		action << " looses " B_YELLOW << amount << RESET << " hit points" << '\n';
	}
	else if (_hit <= 0)
		action << " cannot take damage because " B_RED " is out of hit" RESET "\n";

	state << B_BLUE << _hit << "," << _energy << "," << _attack << B_CYAN "]" RESET;
	std::cout << state.str() << action.str();
}

void	ClapTrap::beRepaired(unsigned int amount) {

	std::ostringstream action;
	std::ostringstream state;

	state << B_CYAN "[" B_GREEN << _class[0] << _number << B_PURPLE "h,e,a" B_CYAN ":" B_BLUE << _hit << "," << _energy << "," << _attack << B_CYAN "->";
	action << _class << " " << _name << "-" << _number;

	if (_energy > 0 && _hit > 0)
	{
		_energy--;
		if (_energy < 0)
			_energy = 0;
		_hit += amount;
		action << " repaired itself and gained " B_YELLOW << amount << RESET << " points of life" << '\n';
	}
	else
	{
		if (_energy <= 0)
			action << " cannot repair itself because " B_RED " is out of energy" RESET "\n";
		else if (_hit <= 0)
			action << " cannot repair itself because " B_RED " is out of hit" RESET "\n";
	}

	state << B_BLUE << _hit << "," << _energy << "," << _attack << B_CYAN "]" RESET;
	std::cout << state.str() << action.str();
}

/*********************************************
 * STATICS
 *********************************************/

int	ClapTrap::_totalNumber = 0;

std::string const	ClapTrap::_dName = "robot";
std::string const	ClapTrap::_dClass = "ClapTrap";
int const			ClapTrap::_dHit = 10;
int const			ClapTrap::_dEnergy = 10;
int const			ClapTrap::_dAttack = 0;

