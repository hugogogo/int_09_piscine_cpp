#include "ClapTrap.hpp"

int	main() {

	ClapTrap robot1("robot1");
	ClapTrap robot2("robot2");

	robot1.attack(robot2.getName());
	robot2.takeDamage(robot1.getAttack());
	robot2.beRepaired(robot1.getAttack());

	robot2.attack(robot1.getName());
	robot1.takeDamage(robot2.getAttack());
	robot1.beRepaired(robot2.getAttack());

	robot1.attack(robot2.getName());
	robot2.takeDamage(robot1.getAttack());
	robot2.beRepaired(robot1.getAttack());

	robot2.attack(robot1.getName());
	robot1.takeDamage(robot2.getAttack());
	robot1.beRepaired(robot2.getAttack());

	robot1.attack(robot2.getName());
	robot2.takeDamage(robot1.getAttack());
	robot2.beRepaired(robot1.getAttack());

	robot2.attack(robot1.getName());
	robot1.takeDamage(robot2.getAttack());
	robot1.beRepaired(robot2.getAttack());

	robot1.attack(robot2.getName());
	robot2.takeDamage(robot1.getAttack());
	robot2.beRepaired(robot1.getAttack());

	robot2.attack(robot1.getName());
	robot1.takeDamage(robot2.getAttack());
	robot1.beRepaired(robot2.getAttack());

	robot1.attack(robot2.getName());
	robot2.takeDamage(robot1.getAttack());
	robot2.beRepaired(robot1.getAttack());

	robot2.attack(robot1.getName());
	robot1.takeDamage(robot2.getAttack());
	robot1.beRepaired(robot2.getAttack());

	robot1.attack(robot2.getName());
	robot2.takeDamage(robot1.getAttack());
	robot2.beRepaired(robot1.getAttack());

	return 0;
}
