#include "ScavTrap.hpp"

/*********************************************
 * CONSTRUCTORS
 *********************************************/

ScavTrap::ScavTrap( std::string name ) : ClapTrap(name) {
	_class = _dClass;
	_hit = _dHit;
	_energy = _dEnergy;
	_attack = _dAttack;
	std::cout << _class << " " << _name << "-" << _number << " created\n";
	return;
}

ScavTrap::ScavTrap( ScavTrap const & src ) : ClapTrap(src) {
	_class = _dClass;
	*this = src;
	_number = getTotalNumber();
	std::cout << _class << " " << _name << "-" << _number << " copied from " << src._class << "-" << src._name << "-" << src._number << "\n";
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

ScavTrap::~ScavTrap( void ) {
	std::cout << _class << " " << _name << "-" << _number << " destructed\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

ScavTrap &	ScavTrap::operator=( ScavTrap const & rhs ) {
	ClapTrap::operator=(rhs);
	return *this;
}

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	ScavTrap::attack(const std::string & target) {

	std::ostringstream action;
	std::ostringstream state;

	state << B_CYAN "[" B_GREEN << _class[0] << _number << B_PURPLE "h,e,a" B_CYAN ":" B_BLUE << _hit << "," << _energy << "," << _attack << B_CYAN "->";
	action << _class << " " << _name << "-" << _number << B_CYAN " SPECIAL SCAV" RESET;

	if (_energy > 0 && _hit > 0)
	{
		_energy--;
		if (_energy < 0)
			_energy = 0;
		action << " attacked " << target << ", causing " B_YELLOW << _attack << RESET << " points of damage" << '\n';
	}
	else
	{
		_attack = 0;
		if (_energy <= 0)
			action << " cannot attack because " B_RED " is out of energy" RESET"\n";
		else if (_hit <= 0)
			action << " cannot attack because " B_RED " is out of hit" RESET "\n";
	}

	state << B_BLUE << _hit << "," << _energy << "," << _attack << B_CYAN "]" RESET;
	std::cout << state.str() << action.str();
}

void	ScavTrap::guardGate() {
	std::cout << _class << " " << _name << "-" << _number << " entered special mode Gate Keeper\n";
}

/*********************************************
 * STATICS
 *********************************************/

std::string const	ScavTrap::_dName = "robot";
std::string const	ScavTrap::_dClass = "ScavTrap";
int const			ScavTrap::_dHit = 100;
int const			ScavTrap::_dEnergy = 50;
int const			ScavTrap::_dAttack = 20;

