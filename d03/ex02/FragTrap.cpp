#include "FragTrap.hpp"

/*********************************************
 * CONSTRUCTORS
 *********************************************/

FragTrap::FragTrap( std::string name ) : ClapTrap(name) {
	_class = _dClass;
	_hit = _dHit;
	_energy = _dEnergy;
	_attack = _dAttack;
	std::cout << _class << " " << _name << "-" << _number << " created\n";
	return;
}

FragTrap::FragTrap( FragTrap const & src ) : ClapTrap(src) {
	_class = _dClass;
	*this = src;
	_number = getTotalNumber();
	std::cout << _class << " " << _name << "-" << _number << " copied from " << src._class << "-" << src._name << "-" << src._number << "\n";
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

FragTrap::~FragTrap( void ) {
	std::cout << _class << " " << FragTrap::_name << " destructed\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

FragTrap &	FragTrap::operator=( FragTrap const & rhs ) {
	ClapTrap::operator=(rhs);
	return *this;
}

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	FragTrap::highFivesGuys() {
	std::cout << _class << " " << FragTrap::_name << " wait for a high fives, common guys\n";
}

/*********************************************
 * STATICS
 *********************************************/

std::string const	FragTrap::_dName = "robot";
std::string const	FragTrap::_dClass = "FragTrap";
int const			FragTrap::_dHit = 100;
int const			FragTrap::_dEnergy = 100;
int const			FragTrap::_dAttack = 50;

