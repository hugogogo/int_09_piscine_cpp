#include "FragTrap.hpp"

/*********************************************
 * CONSTRUCTORS
 *********************************************/

FragTrap::FragTrap( std::string name ) : ClapTrap(name) {
	_class = _dClass;
	_hit = _dHit;
	_energy = _dEnergy;
	_attack = _dAttack;
	std::cout << _class << " " << _name << "-" << _number << " created\n";
	return;
}

FragTrap::FragTrap( FragTrap const & src ) : ClapTrap(src) {
	_class = _dClass;
	*this = src;
	_number = getTotalNumber();
	std::cout << _class << " " << _name << "-" << _number << " copied from " << src._class << "-" << src._name << "-" << src._number << "\n";
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

FragTrap::~FragTrap( void ) {
	std::cout << _class << " " << _name << "-" << _number << " destructed\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

FragTrap &	FragTrap::operator=( FragTrap const & rhs ) {
	ClapTrap::operator=(rhs);
	return *this;
}

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

//void	FragTrap::attack(const std::string & target) {
//
//	std::ostringstream action;
//	std::ostringstream state;
//
//	state << B_CYAN "[" B_GREEN << _class[0] << _number << B_PURPLE "h,e,a" B_CYAN ":" B_BLUE << _hit << "," << _energy << "," << _attack << B_CYAN "->";
//	action << _class << " " << _name << "-" << _number << B_CYAN " SPECIAL FRAG" RESET;
//
//	if (_energy > 0 && _hit > 0)
//	{
//		_energy--;
//		if (_energy < 0)
//			_energy = 0;
//		action << " attacked " << target << ", causing " B_YELLOW << _attack << RESET << " points of damage" << '\n';
//	}
//	else
//	{
//		_attack = 0;
//		if (_energy <= 0)
//			action << " cannot attack because " B_RED " is out of energy" RESET"\n";
//		else if (_hit <= 0)
//			action << " cannot attack because " B_RED " is out of hit" RESET "\n";
//	}
//
//	state << B_BLUE << _hit << "," << _energy << "," << _attack << B_CYAN "]" RESET;
//	std::cout << state.str() << action.str();
//}

void	FragTrap::highFivesGuys() {
	std::cout << _class << " " << _name << "-" << _number << " wait for a high fives, common guys\n";
}

/*********************************************
 * STATICS
 *********************************************/

std::string const	FragTrap::_dName = "robot";
std::string const	FragTrap::_dClass = "FragTrap";
int const			FragTrap::_dHit = 100;
int const			FragTrap::_dEnergy = 100;
int const			FragTrap::_dAttack = 30;

