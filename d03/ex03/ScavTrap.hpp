#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

#include <iostream>
#include <string>
#include "ClapTrap.hpp"

#ifndef COLORS_MACRO
# define COLORS_MACRO

# define B_GRAY "\e[1;30m"
# define B_RED "\e[1;31m"
# define B_GREEN "\e[1;32m"
# define B_YELLOW "\e[1;33m"
# define B_BLUE "\e[1;34m"
# define B_PURPLE "\e[1;35m"
# define B_CYAN "\e[1;36m"
# define B_WHITE "\e[1;37m"

# define RESET "\e[0m"

#endif

class ScavTrap : public virtual ClapTrap {

public:

	ScavTrap( std::string name = ScavTrap::_dName );
	ScavTrap( ScavTrap const & src );				// copy constructor
	~ScavTrap();									// destructor

	ScavTrap &	operator=( ScavTrap const & rhs );	// assignement operator

	void	guardGate();
	void	attack(const std::string & target);

protected:

	static const std::string	_dName;
	static const std::string	_dClass;
	static const int			_dHit;
	static const int			_dEnergy;
	static const int			_dAttack;
	static const int			_dNumber;

};

#endif

