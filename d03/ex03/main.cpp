#include "ClapTrap.hpp"
#include "ScavTrap.hpp"
#include "FragTrap.hpp"
#include "DiamondTrap.hpp"

#include <iostream>

template < typename U, typename V >
void	goAttack(U & robot1, V & robot2) {

	std::cout << ".";
	robot1.attack(robot2.getName());
	std::cout << " ";
	robot2.takeDamage(robot1.getAttack());
	std::cout << " ";
	robot2.beRepaired(robot1.getAttack());

}

int	main() {

	FragTrap robot1("robot1");
	ClapTrap robot2("robot2");
	ScavTrap robot3("robot3");
	std::cout << "start\n";
	DiamondTrap robot4("robot4");
	std::cout << "end\n";
	ClapTrap robot5("robot5");
	ScavTrap robot6("robot6");
	FragTrap robot7("robot7");
	FragTrap robot8("robot8");
	ClapTrap robot9("robot9");

	goAttack(robot1, robot2);
	goAttack(robot6, robot3);
	goAttack(robot6, robot7);
	goAttack(robot8, robot1);
	goAttack(robot1, robot3);
robot4.guardGate();
	goAttack(robot1, robot4);
	goAttack(robot9, robot2);
	goAttack(robot1, robot5);
	goAttack(robot3, robot2);
	goAttack(robot4, robot8);
robot8.highFivesGuys();
	goAttack(robot2, robot7);
	goAttack(robot3, robot2);
	goAttack(robot1, robot2);
	goAttack(robot8, robot4);
	goAttack(robot2, robot4);
robot3.guardGate();
	goAttack(robot3, robot1);
	goAttack(robot5, robot7);
	goAttack(robot1, robot9);
	goAttack(robot9, robot6);
	goAttack(robot9, robot2);
robot6.guardGate();
robot1.highFivesGuys();
robot7.highFivesGuys();
	goAttack(robot3, robot4);
	goAttack(robot5, robot2);
	goAttack(robot7, robot6);
	goAttack(robot2, robot1);
robot7.highFivesGuys();
	goAttack(robot2, robot4);
	goAttack(robot9, robot6);
	goAttack(robot3, robot2);
robot4.whoAmI();
	goAttack(robot6, robot5);
	goAttack(robot4, robot9);
	goAttack(robot7, robot6);

	return 0;
}
