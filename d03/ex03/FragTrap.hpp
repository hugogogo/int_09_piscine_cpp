#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

#include <iostream>
#include <string>
#include "ClapTrap.hpp"

class FragTrap : public virtual ClapTrap {

public:

	FragTrap( std::string name = FragTrap::_dName );
	FragTrap( FragTrap const & src );				// copy constructor
	~FragTrap();									// destructor

	FragTrap &	operator=( FragTrap const & rhs );	// assignement operator

	void	highFivesGuys();
//	void	attack(const std::string & target);

protected:

	static const std::string	_dName;
	static const std::string	_dClass;
	static const int			_dHit;
	static const int			_dEnergy;
	static const int			_dAttack;
	static const int			_dNumber;

};

#endif
