#include "DiamondTrap.hpp"

/*********************************************
 * CONSTRUCTORS
 *********************************************/

DiamondTrap::DiamondTrap( std::string name )
: ClapTrap(name + "_clap_name") {
	_name = name;
	_class = _dClass;
	_hit = _dHit;
	_energy = _dEnergy;
	_attack = _dAttack;
	std::cout << _class << " " << _name << "-" << _number << " created\n";
	return;
}

DiamondTrap::DiamondTrap( DiamondTrap const & src )
: ClapTrap(src)
, FragTrap(src)
, ScavTrap(src) {
	_class = _dClass;
	*this = src;
	_number = getTotalNumber();
	std::cout << _class << " " << _name << "-" << _number << " copied from " << src._class << "-" << src._name << "-" << src._number << "\n";
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

DiamondTrap::~DiamondTrap( void ) {
	std::cout << _class << " " << _name << "-" << _number << " destructed\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

DiamondTrap &	DiamondTrap::operator=( DiamondTrap const & rhs ) {
	ClapTrap::operator=(rhs);
	return *this;
}

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	DiamondTrap::whoAmI() {
	std::cout << _class << B_CYAN " I AM DIAMOND" RESET << " " << _name << "-" << _number;
	std::cout << B_CYAN " and clap" RESET << " " << ClapTrap::_name << "-" << _number << "\n";
}

/*********************************************
 * STATICS
 *********************************************/

std::string const	DiamondTrap::_dName = "robot";
std::string const	DiamondTrap::_dClass = "DiamondTrap";
int const			DiamondTrap::_dHit = FragTrap::_dHit;
int const			DiamondTrap::_dEnergy = ScavTrap::_dEnergy;
int const			DiamondTrap::_dAttack = FragTrap::_dAttack;

