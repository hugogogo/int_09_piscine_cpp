#include "ClapTrap.hpp"
#include "ScavTrap.hpp"

#include <iostream>
#include <sstream>

# define B_GRAY "\e[1;30m"
# define B_RED "\e[1;31m"
# define B_GREEN "\e[1;32m"
# define B_YELLOW "\e[1;33m"
# define B_BLUE "\e[1;34m"
# define B_PURPLE "\e[1;35m"
# define B_CYAN "\e[1;36m"
# define B_WHITE "\e[1;37m"

# define RESET "\e[0m"

# define COLOR1 B_YELLOW

template < typename U, typename V >
void	goAttack(U & robot1, V & robot2) {
	std::stringstream ss;
	std::string num;
	ss << robot2.getNumber();
	ss >> num;
	std::cout << ".";
	robot1.attack(robot2.getName() + "-" + num);
	std::cout << " ";
	robot2.takeDamage(robot1.getAttack());
	std::cout << " ";
	robot2.beRepaired(robot1.getAttack());
}

int	main() {

	ClapTrap robot1("robot1");
	ScavTrap robot2("robot2");
	robot2.guardGate();
	ScavTrap robot3("robot3");
	ClapTrap robot4("robot4");

std::cout << COLOR1 "\nassignement 1 (ClapTrap(ScavTrap)):\n" RESET;
	ClapTrap robot5(robot2);
//	robot5.guardGate();
	goAttack(robot5, robot2);

std::cout << COLOR1 "\nassignement 2 (ScavTrap(ScavTrap)):\n" RESET;
	ScavTrap robot6(robot2);
	robot6.guardGate();
	goAttack(robot6, robot2);
	robot2.takeDamage(robot6.getAttack());
	robot6 = robot2;
	goAttack(robot6, robot2);

std::cout << COLOR1 "\nassignement 3 (ScavTrap(ClapTrap)):\n" RESET;
//	ScavTrap robot7(robot1);

std::cout << COLOR1 "\nassignement 4 (ClapTrap(ClapTrap)):\n" RESET;
	ClapTrap robot7(robot1);
	goAttack(robot7, robot1);

std::cout << COLOR1 "\nwar:\n" RESET;
	goAttack(robot1, robot2);
	goAttack(robot2, robot1);
	goAttack(robot1, robot3);
	robot2.guardGate();
	goAttack(robot1, robot4);
	goAttack(robot4, robot2);
	goAttack(robot2, robot3);
	goAttack(robot2, robot4);
	robot3.guardGate();
	goAttack(robot3, robot1);
	robot2.guardGate();
	goAttack(robot3, robot4);
	goAttack(robot2, robot1);
	goAttack(robot2, robot4);
	goAttack(robot1, robot3);

std::cout << COLOR1 "\ndestruction:\n" RESET;

	return 0;
}
