#include <iostream>
#include <string>
#include "colors.h"
#include "Templates.hpp"
#include "Fixed.hpp"

#define N_TEST "5"

class Test {
public:
	Test(int n = 0) : _n(n) {}
	int	getN() const {return _n;}
	bool	operator==(Test const & rhs) const {return (this->_n == rhs._n);}
	bool	operator!=(Test const & rhs) const {return (this->_n != rhs._n);}
	bool	operator> (Test const & rhs) const {return (this->_n > rhs._n);}
	bool	operator< (Test const & rhs) const {return (rhs._n > this->_n);}
	bool	operator>=(Test const & rhs) const {return ( !(this->_n < rhs._n) );}
	bool	operator<=(Test const & rhs) const {return ( !(this->_n > rhs._n) );}
private:
	int _n;
};
std::ostream &	operator<<(std::ostream & o, Test const & rhs){
	o << rhs.getN();
	return (o);
}

int	main() {
	int	i = 0;

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "tests int :" RESET "\n";
	{
		int a = 2;
		int b = 3;

		std::cout << "a = " << a << ", b = " << b << "\n" B_BLUE "swap :" RESET "\n";
		::swap( a, b );
		std::cout << "a = " << a << ", b = " << b << "\n";

		std::cout << B_BLUE "min( a, b ) = " RESET << ::min( a, b ) << "\n";
		std::cout << B_BLUE "max( a, b ) = " RESET << ::max( a, b ) << "\n";
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "tests string :" RESET "\n";
	{
		std::string a = "chaine1";
		std::string b = "chaine2";

		std::cout << "a = " << a << ", b = " << b << "\n" B_BLUE "swap :" RESET "\n";
		::swap(a, b);
		std::cout << "a = " << a << ", b = " << b << "\n";

		std::cout << B_BLUE "min( a, b ) = " RESET << ::min( a, b ) << "\n";
		std::cout << B_BLUE "max( a, b ) = " RESET << ::max( a, b ) << "\n";
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "tests floats :" RESET "\n";
	{
		float a = 2.42f;
		float b = 32.7f;

		std::cout << "a = " << a << ", b = " << b << "\n" B_BLUE "swap :" RESET "\n";
		::swap( a, b );
		std::cout << "a = " << a << ", b = " << b << "\n";

		std::cout << B_BLUE "min( a, b ) = " RESET << ::min( a, b ) << "\n";
		std::cout << B_BLUE "max( a, b ) = " RESET << ::max( a, b ) << "\n";
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "tests fixed :" RESET "\n";
	{
		Fixed a(2.42f);
		Fixed b(32.7f);

		std::cout << "a = " << a << ", b = " << b << "\n" B_BLUE "swap :" RESET "\n";
		::swap( a, b );
		std::cout << "a = " << a << ", b = " << b << "\n";

		std::cout << B_BLUE "min( a, b ) = " RESET << ::min( a, b ) << "\n";
		std::cout << B_BLUE "max( a, b ) = " RESET << ::max( a, b ) << "\n";
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "tests class :" RESET "\n";
	{

		Test a(8);
		Test b(16);

		std::cout << "a = " << a << ", b = " << b << "\n" B_BLUE "swap :" RESET "\n";
		::swap( a, b );
		std::cout << "a = " << a << ", b = " << b << "\n";

		std::cout << B_BLUE "min( a, b ) = " RESET << ::min( a, b ) << "\n";
		std::cout << B_BLUE "max( a, b ) = " RESET << ::max( a, b ) << "\n";
	}

	return 0;
}

