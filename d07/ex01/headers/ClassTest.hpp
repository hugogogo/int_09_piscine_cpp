#ifndef CLASSTEST_HPP
# define CLASSTEST_HPP

# include <iostream>

class ClassTest {
public:
	ClassTest() : _value("hello") {}
	std::string	getValue() const {return _value;}
private:
	std::string	_value;
};

std::ostream & operator<<(std::ostream & o, ClassTest const & rhs) {
	o << rhs.getValue();
	return o;
}

#endif

