#ifndef TOUPPER_HPP
# define TOUPPER_HPP

# include <iostream>
# include <string>
# include <cctype>

template< typename T >
void	ToUpper(T & e) {
	e = toupper(e);
}

#endif

