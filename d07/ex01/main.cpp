#include <iostream>
#include <string>
#include "colors.h"
#include "Iter.hpp"
#include "Print.hpp"
#include "ToUpper.hpp"
#include "Decrement.hpp"
#include "ClassTest.hpp"
#include "Fixed.hpp"

#define N_TEST "4"

int	main() {
	int	i = 0;

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "tests char :" RESET "\n";
	{
		char	arr[] = "abracadabra";
		size_t	len = sizeof(arr) / sizeof(arr[0]);
		
		::Iter(arr, len, Print<char>);
		std::cout << B_BLUE "ToUpper :" RESET "\n";
		::Iter(arr, len, ToUpper<char>);
		::Iter(arr, len, Print<char>);
		std::cout << B_BLUE "Decrement :" RESET "\n";
		::Iter(arr, len, Decrement<char>);
		::Iter(arr, len, Print<char>);
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "tests int :" RESET "\n";
	{
		int		arr[] = {1256, 23, 4352, -3287, 0, 24, 4376382};
		size_t	len = sizeof(arr) / sizeof(arr[0]);
		
		::Iter(arr, len, Print<int>);
		std::cout << B_BLUE "Decrement :" RESET "\n";
		::Iter(arr, len, Decrement<int>);
		::Iter(arr, len, Print<int>);
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "tests class :" RESET "\n";
	{
		ClassTest tab[5];

		::Iter(tab, 5, Print<ClassTest>);
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "tests fixed :" RESET "\n";
	{
		Fixed f[5];

		::Iter(f, 5, Print<Fixed>);
	}

	return 0;
}

