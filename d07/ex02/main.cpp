#include <iostream>
#include <string>
#include <cstdlib> // rand()
#include "colors.h"

#include "Iter.hpp"
#include "Print.hpp"
#include "Array.hpp"
#include "Fill.hpp"
#include "Fixed.hpp"

#define N_TEST "13"

int	main() {
	srand(time(NULL));
    int i = 0;

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "tests basic copy :" RESET "\n";
    {
        Array<int> a(13);
		Array<int> b(a);

		::Fill(a, a.size(), 0);
		::Fill(b, b.size(), 0);

        std::cout << "a: ";
		::Iter(a, a.size(), Print<int>);
        std::cout << "\n";

        std::cout << "b: ";
		::Iter(b, b.size(), Print<int>);
        std::cout << "\n";

		::Fill(a, a.size(), 2);
        std::cout << B_BLUE "\na filled with 2" RESET "\n";

        std::cout << "a: ";
		::Iter(a, a.size(), Print<int>);
        std::cout << "\n";

        std::cout << "b: ";
		::Iter(b, b.size(), Print<int>);
        std::cout << "\n";

        b = a;
		::Fill(a, a.size(), 3);
        std::cout << B_BLUE "\nb = a\na filled with 3" RESET "\n";

        std::cout << "a: ";
		::Iter(a, a.size(), Print<int>);
        std::cout << "\n";

        std::cout << "b: ";
		::Iter(b, b.size(), Print<int>);
        std::cout << "\n";
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "tests copy  with different size:" RESET "\n";
    {
        Array<int> a(17);
		Array<int> b(13);

		::Fill(a, a.size(), 2);
		::Fill(b, b.size(), 3);
        std::cout << B_BLUE "\na filled with 2" RESET "\n";
        std::cout << B_BLUE "\nb filled with 3" RESET "\n";

        std::cout << "a: ";
		::Iter(a, a.size(), Print<int>);
        std::cout << "\nb: ";
		::Iter(b, b.size(), Print<int>);
        std::cout << "\n";

        b = a;
        std::cout << B_BLUE "\nb = a" RESET "\n";

        std::cout << "a: ";
		::Iter(a, a.size(), Print<int>);
        std::cout << "\nb: ";
		::Iter(b, b.size(), Print<int>);
        std::cout << "\n";

		::Fill(b, b.size(), 4);
        std::cout << B_BLUE "\nb filled with 4" RESET "\n";

        std::cout << "a: ";
		::Iter(a, a.size(), Print<int>);
        std::cout << "\nb: ";
		::Iter(b, b.size(), Print<int>);
        std::cout << "\n";
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "tests simple array :" RESET "\n";
	{
		Array<int>	a(10);

		std::cout << "a.size :" << a.size() << "\n";
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "tests empty array :" RESET "\n";
	{
		Array<int>	arr(0);

		std::cout << "arr.size :" << arr.size() << "\n";
		try {
			arr[arr.size()] = 42; }
		catch(std::exception& e) {
			std::cerr << e.what() << '\n'; }
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "tests write and read :" RESET "\n";
	{
		std::cout << B_BLUE "create Array<int> arr(5);" RESET "\n";
		Array<int>	arr(5);
		std::cout << B_BLUE "create Array<int> const constarr(5);" RESET "\n";
		Array<int> const	constarr(5);

		std::cout << "arr.size :" << arr.size() << "\n";
		std::cout << "constarr.size :" << constarr.size() << "\n";

		try {
			std::cout << B_BLUE "arr[3] = 12" RESET "\n";
			arr[3] = 12;
			std::cout << B_BLUE "print arr;" RESET "\n";
			::Iter(arr, arr.size(), Print<int>);
			std::cout << "\n"; }
		catch(std::exception& e) {
			std::cerr << e.what() << '\n'; }

		try {
			std::cout << B_BLUE "print constarr;" RESET "\n";
			::Iter(constarr, constarr.size(), Print<int>);
			std::cout << "\n"; }
		catch(std::exception& e) {
			std::cerr << e.what() << '\n'; }
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "tests empty Fixed array :" RESET "\n";
	{
		Array<Fixed>	arr(0);

		std::cout << "fixed arr.size :" << arr.size() << "\n";
		try {
			arr[arr.size()] = 42; }
		catch(std::exception& e) {
			std::cerr << e.what() << '\n'; }
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "tests fixed write and read :" RESET "\n";
	{
		std::cout << B_BLUE "create Array<Fixed> arr(5);" RESET "\n";
		Array<Fixed>	arr(5);
		std::cout << B_BLUE "create Array<Fixed> const constarr(5);" RESET "\n";
		Array<Fixed> const	constarr(5);

		std::cout << "arr.size :" << arr.size() << "\n";
		std::cout << "constarr.size :" << constarr.size() << "\n";

		try {
			std::cout << B_BLUE "arr[3] = 12" RESET "\n";
			arr[3] = 12;
			std::cout << B_BLUE "print arr;" RESET "\n";
			::Iter(arr, arr.size(), Print<Fixed>);
			std::cout << "\n"; }
		catch(std::exception& e) {
			std::cerr << e.what() << '\n'; }

		try {
			std::cout << B_BLUE "print constarr;" RESET "\n";
			::Iter(constarr, constarr.size(), Print<Fixed>);
			std::cout << "\n"; }
		catch(std::exception& e) {
			std::cerr << e.what() << '\n'; }
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "tests empty Fixed array :" RESET "\n";
	{
		Array<char>	arr(0);

		std::cout << "char arr.size :" << arr.size() << "\n";
		try {
			arr[arr.size()] = 'c'; }
		catch(std::exception& e) {
			std::cerr << e.what() << '\n'; }
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "tests char write and read :" RESET "\n";
	{
		std::cout << B_BLUE "create Array<char> arr(5);" RESET "\n";
		Array<char>	arr(5);
		std::cout << B_BLUE "create Array<char> const constarr(5);" RESET "\n";
		Array<char> const	constarr(5);

		std::cout << "arr.size :" << arr.size() << "\n";
		std::cout << "constarr.size :" << constarr.size() << "\n";

		try {
			std::cout << B_BLUE "arr[3] = 12" RESET "\n";
			arr[3] = 12;
			std::cout << B_BLUE "print arr;" RESET "\n";
			::Iter(arr, arr.size(), Print<char>);
			std::cout << "\n"; }
		catch(std::exception& e) {
			std::cerr << e.what() << '\n'; }

		try {
			std::cout << B_BLUE "print constarr;" RESET "\n";
			::Iter(constarr, constarr.size(), Print<char>);
			std::cout << "\n"; }
		catch(std::exception& e) {
			std::cerr << e.what() << '\n'; }
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "tests index correct :" RESET "\n";
	{
		unsigned int	l = 162;
		Array<int>	arr(l);

		::Fill(arr, arr.size(), 0);

		try {
			arr[42] = 42; }
		catch(std::exception& e) {
			std::cerr << e.what() << '\n'; }

        std::cout << "print arr: ";
		::Iter(arr, l, Print<int>);
        std::cout << "\n";
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "tests index < 0 :" RESET "\n";
	{
		Array<int>	arr(162);

		try {
			arr[-42] = 42; }
		catch(std::exception& e) {
			std::cerr << e.what() << '\n'; }
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "tests index >= array.size :" RESET "\n";
	{
		Array<int>	arr(59);

		try {
			arr[arr.size()] = 42; }
		catch(std::exception& e) {
			std::cerr << e.what() << '\n'; }
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "tests subject :" RESET "\n";
	{
		#define MAX_VAL 750
		Array<int> numbers(MAX_VAL);
		int* mirror = new int[MAX_VAL];
		srand(time(NULL));
		for (int i = 0; i < MAX_VAL; i++)
		{
		    const int value = rand();
		    numbers[i] = value;
		    mirror[i] = value;
		}
		//SCOPE
		{
		    Array<int> tmp = numbers;
		    Array<int> test(tmp);
		}
		
		for (int i = 0; i < MAX_VAL; i++)
		{
		    if (mirror[i] != numbers[i])
		    {
		        std::cerr << "didn't save the same value!!" << std::endl;
		        return 1;
		    }
		}
		try
		{
		    numbers[-2] = 0;
		}
		catch(const std::exception& e)
		{
		    std::cerr << e.what() << '\n';
		}
		try
		{
		    numbers[MAX_VAL] = 0;
		}
		catch(const std::exception& e)
		{
		    std::cerr << e.what() << '\n';
		}
		
		for (int i = 0; i < MAX_VAL; i++)
		{
		    numbers[i] = rand();
		}
		delete [] mirror;//
	}

	return 0;
}

