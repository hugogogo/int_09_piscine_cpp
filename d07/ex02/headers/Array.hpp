#ifndef ARRAY_HPP
# define ARRAY_HPP

#include <iostream>

template < typename T >
class Array {
public:
	Array(unsigned int n = 0U)
	: _size(n)
	, _arr(new T[n]){
		for (unsigned int i = 0; i < n; i++)
			_arr[i] = 0;
	};

	~Array() {
		delete [] _arr;
	};

	Array(Array const & src)
	: _arr(new T[0]) {
		*this = src;
	};

	Array &operator=(Array const & rhs) {
		if (this == &rhs)
			return (*this);
		delete[] _arr;
		_arr = new T[rhs._size];
		_size = rhs._size;
		for (unsigned int it = 0; it < rhs._size && it < _size; it++)
			_arr[it] = rhs._arr[it];
		return (*this);
	};

	// seen on luke's code :
	// https://isocpp.org/wiki/faq/const-correctness#const-overloading
	T &	operator[](size_t i) {
		if (i < 0 || i >= _size)
			throw ArrayOutOfRangeException();
		return (_arr[i]);
	};
	const T &	operator[](size_t i) const {
		if (i < 0 || i >= _size)
			throw ArrayOutOfRangeException();
		return (_arr[i]);
	};

	size_t	size() const { return (_size); };

private:

	class ArrayOutOfRangeException : public std::exception {
		const char	*what() const throw() {
			return ("out of range array index");};
	};
	size_t	_size;
	T	*	_arr;
};

#endif
