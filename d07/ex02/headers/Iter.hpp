#ifndef ITER_HPP
# define ITER_HPP

template< typename T, typename F >
void	Iter(T & arr, size_t len, F & f)
{
	for (size_t i = 0;  i < len; i++)
		f(arr[i]);
}

#endif
