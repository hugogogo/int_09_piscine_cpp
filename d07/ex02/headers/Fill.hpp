#ifndef FILL_HPP
# define FILL_HPP

# include <iostream>
# include <string>
# include "Iter.hpp"

template< typename T, typename U >
void	Fill(T & arr, size_t len, U const & e) {
	for (size_t i = 0;  i < len; i++)
		arr[i] = e;
}

#endif

