#ifndef FIXED_HPP
# define FIXED_HPP

#include <iostream>
#include <string>
#include <cmath>

class Fixed {

public:

	 Fixed(void);							// default/parametric constructor
	 Fixed(Fixed const & src);				// copy constructor
	~Fixed(void);							// destructor
	 Fixed(int integer);
	 Fixed(float const floater);

	Fixed &	operator= (Fixed const & rhs);	// assignement operator
	bool	operator< (Fixed const & rhs) const;
	bool	operator> (Fixed const & rhs) const;
	bool	operator<=(Fixed const & rhs) const;
	bool	operator>=(Fixed const & rhs) const;
	bool	operator==(Fixed const & rhs) const;
	bool	operator!=(Fixed const & rhs) const;
	Fixed	operator+ (Fixed const & rhs) const;
	Fixed	operator- (Fixed const & rhs) const;
	Fixed	operator* (Fixed const & rhs) const;
	Fixed	operator/ (Fixed const & rhs) const;
	Fixed &	operator++(void); // prefix ++o
	Fixed &	operator--(void); // prefix --o
	Fixed	operator++(int); // postfix o++
	Fixed	operator--(int); // postfix o--

	static const Fixed &	min(Fixed const & lhs, Fixed const & rhs);
	static const Fixed &	max(Fixed const & lhs, Fixed const & rhs);
	static Fixed &			min(Fixed & lhs, Fixed & rhs);
	static Fixed &			max(Fixed & lhs, Fixed & rhs);

	int		getRawBits(void) const;
	void	setRawBits(int const raw);
	float	toFloat(void) const;
	int		toInt(void) const;

private:

	int					_value;
	static int const	_frac;
	static int const	_max;
	static int const	_min;

};

std::ostream &	operator<<(std::ostream & o, Fixed const & rhs);

#endif
