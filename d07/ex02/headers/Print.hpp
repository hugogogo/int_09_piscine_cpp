#ifndef PRINT_HPP
# define PRINT_HPP

# include <iostream>
# include <string>

template< typename T >
void	Print(T const & e) {
	std::cout << "[" << e << "]";
}

#endif
