#include "Dog.hpp"

#define COPLIEN_COLOR B_CYAN

/*********************************************
 * CONSTRUCTORS
 *********************************************/

Dog::Dog() {
	std::cout << COPLIEN_COLOR "Dog constructor" RESET "\n";
	type = "dog";
	_brain = new Brain();
	return;
}

Dog::Dog( Brain *brain ) {
	std::cout << COPLIEN_COLOR "Dog constructor" RESET "\n";
	type = "dog";
	_brain = new Brain();
	*_brain = *brain;
	return;
}

/*
error: base class ‘class Animal’ should be explicitly initialized in the copy constructor [-Werror=extra]
Dog::Dog( Dog const & src ) {
^~~
answer : https://stackoverflow.com/questions/43612772/base-class-class-a-should-be-explicitly-initialized-in-the-copy-constructor
*/
Dog::Dog( Dog const & src ) : Animal() {
	std::cout << COPLIEN_COLOR "Dog copy constructor" RESET "\n";
	*this = src;
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

Dog::~Dog() {
	std::cout << COPLIEN_COLOR "Dog destructor" RESET "\n";
	delete _brain;
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

Dog &	Dog::operator=( Dog const & rhs ) {
	Animal::operator=(rhs);
	_brain = new Brain();
	if (this != &rhs)
		_brain = rhs._brain;
	return *this;
}

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	Dog::makeSound() const {
	std::cout << "*woof*\n";
}

