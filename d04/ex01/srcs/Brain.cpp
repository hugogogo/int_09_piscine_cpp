#include "Brain.hpp"

#define COPLIEN_COLOR B_CYAN

/*********************************************
 * CONSTRUCTORS
 *********************************************/

Brain::Brain() {
	std::cout << COPLIEN_COLOR "Brain constructor" RESET "\n";
	return;
}

Brain::Brain( Brain const & src ) {
	std::cout << COPLIEN_COLOR "Brain copy constructor" RESET "\n";
	*this = src;
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

Brain::~Brain() {
	std::cout << COPLIEN_COLOR "Brain destructor" RESET "\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

Brain &	Brain::operator=( Brain const & rhs ) {
	std::cout << COPLIEN_COLOR "Brain assignator" RESET "\n";
	if ( this != &rhs )
		for (int i = 0; i < SIZE_IDEAS; i++)
			_ideas[i] = rhs._ideas[i];
	return *this;
}

/*********************************************
 * ACCESSORS
 *********************************************/

void	Brain::printIdea(int pos) {
	if (pos < SIZE_IDEAS)
		std::cout << _ideas[pos] << "\n";
}

void	Brain::putIdea(int pos, std::string idea) {
	if (pos < SIZE_IDEAS)
		_ideas[pos] = idea;
}

void	Brain::printIdeas() {
	for (int i = 0; i < SIZE_IDEAS; i++)
		std::cout << _ideas[i] << " - ";
	std::cout << "\n";
}

void	Brain::putIdeas(std::string idea) {
	for (int i = 0; i < SIZE_IDEAS; i++)
		_ideas[i] = idea;
}

