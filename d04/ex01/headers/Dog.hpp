#ifndef DOG_HPP
# define DOG_HPP

# include "color.h"
# include <iostream>
# include <string>

# include "Animal.hpp"
# include "Brain.hpp"

class Dog : public Animal {

public:

	Dog();
	Dog( Brain *brain );
	Dog( Dog const & src );
	~Dog();
	Dog &	operator=( Dog const & rhs );

	void	makeSound() const;

private:

	Brain	*_brain;

};

#endif

