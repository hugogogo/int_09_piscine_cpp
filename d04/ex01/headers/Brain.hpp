#ifndef BRAIN_HPP
# define BRAIN_HPP

#include "color.h"
#include <iostream>
#include <string>

#define SIZE_IDEAS 100

class Brain {

public:

	Brain( void );
	Brain( Brain const & src );
	~Brain( void );
	Brain &	operator=( Brain const & rhs );

	void	printIdea(int pos);
	void	putIdea(int pos, std::string idea);
	void	printIdeas();
	void	putIdeas(std::string idea);

private:

	std::string	_ideas[SIZE_IDEAS];
};

#endif

