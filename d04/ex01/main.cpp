#include "Animal.hpp"
#include "Dog.hpp"
#include "Cat.hpp"

#include <iostream>
#include <string>

#include "color.h"
#define N_TEST "7"

int	main() {
	std::cout << B_YELLOW "\n[1/" N_TEST "] test subject :" RESET "\n";
	{
		const Animal* j = new Dog();
		const Animal* i = new Cat();
		delete j;//should not create a leak
		delete i;
	}

	std::cout << B_YELLOW "\n[2/" N_TEST "] test with brain :" RESET "\n";
	{
		Dog *	dog;
		Cat *	cat1;
		Cat		cat2;
		Brain *	brain1 = new Brain();
		Brain *	brain2 = new Brain();

			std::cout << B_BLUE "fill brain1 with \"giraffe\" :" RESET "\n";
		brain1->putIdeas("giraffe");
			std::cout << B_BLUE "print brain1 :" RESET "\n";
		brain1->printIdeas();
			std::cout << B_BLUE "print brain2 :" RESET "\n";
		brain2->printIdeas();
			std::cout << B_BLUE "brain2 copy brain1 :" RESET "\n";
		*brain2 = *brain1;
			std::cout << B_BLUE "fill brain1 with \"hippopotamus\" :" RESET "\n";
		brain1->putIdeas("hippopotamus");
			std::cout << B_BLUE "print brain1 :" RESET "\n";
		brain1->printIdeas();
			std::cout << B_BLUE "print brain2 :" RESET "\n";
		brain2->printIdeas();

			std::cout << B_BLUE "create new dog with brain1 :" RESET "\n";
		dog = new Dog(brain1);
			std::cout << B_BLUE "create new cat with brain1 :" RESET "\n";
		cat1 = new Cat(brain1);
			std::cout << B_BLUE "cat2 copy cat1 :" RESET "\n";
		cat2 = *cat1;

			std::cout << B_BLUE "fill brain1 with \"zebra\" :" RESET "\n";
		brain1->putIdeas("zebra");
			std::cout << B_BLUE "print cat1 :" RESET "\n";
		cat1->printBrain();
			std::cout << B_BLUE "print cat2 :" RESET "\n";
		cat2.printBrain();

			std::cout << B_BLUE "delete dog :" RESET "\n";
		delete dog;
			std::cout << B_BLUE "delete cat1 :" RESET "\n";
		delete cat1;
			std::cout << B_BLUE "delete brain1 :" RESET "\n";
		delete brain1;
			std::cout << B_BLUE "delete brain2 :" RESET "\n";
		delete brain2;
	}

	std::cout << B_YELLOW "\n[3/" N_TEST "] array animal test :" RESET "\n";
	{
		Animal	*animals[10];
		int		i;

		for (i = 0 ; i < 5 ; ++i)
			animals[i] = new Cat();
		for ( ; i < 10 ; ++i)
			animals[i] = new Dog();
		for (i = 0 ; i < 10 ; ++i)
			animals[i]->makeSound();
		for (i = 0 ; i < 10 ; ++i)
			delete animals[i];
	}

	std::cout << B_YELLOW "\n[4/" N_TEST "] copy constructor test1/2 :" RESET "\n";
	{
		std::cout << B_BLUE "Cat a_cat :" RESET "\n";
		Cat	a_cat;
		std::cout << B_BLUE "Cat a_cpy_cat(a_cat) :" RESET "\n";
		Cat	a_cpy_cat(a_cat);
	}

	std::cout << B_YELLOW "\n[5/" N_TEST "] copy constructor test2/2 :" RESET "\n";
	{
		std::cout << B_BLUE "Cat a_cat :" RESET "\n";
		Cat	a_cat;
		std::cout << B_BLUE "Cat a_cpy_cat = a_cat :" RESET "\n";
		Cat	a_cpy_cat = a_cat;
	}

	std::cout << B_YELLOW "\n[6/" N_TEST "] assignation operator test1 :" RESET "\n";
	{
		std::cout << B_BLUE "Cat a_cat :" RESET "\n";
		Cat	a_cat;
		std::cout << B_BLUE "Cat a_cpy_cat :" RESET "\n";
		Cat	a_cpy_cat;
		std::cout << B_BLUE "a_cpy_cat = a_cat :" RESET "\n";
		a_cpy_cat = a_cat;
	}

	std::cout << B_YELLOW "\n[7/" N_TEST "] assignation operator test2 :" RESET "\n";
	{
		std::cout << B_BLUE "const Cat *a_cat :" RESET "\n";
		const Cat *a_cat = new Cat();
		std::cout << B_BLUE "Cat a_cpy_cat :" RESET "\n";
		Cat	a_cpy_cat;
		std::cout << B_BLUE "a_cpy_cat = *a_cat :" RESET "\n";
		a_cpy_cat = *a_cat;

		delete a_cat;
	}


	return 0;
}

