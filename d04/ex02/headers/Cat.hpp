#ifndef CAT_HPP
# define CAT_HPP

# include "color.h"
# include <iostream>
# include <string>

# include "Animal.hpp"
# include "Brain.hpp"

class Cat : public Animal {

public:

	Cat();
	Cat( Brain *brain );
//	Cat( Brain *brain = new Brain() );
	Cat( std::string ideas );
	Cat( Cat const & src );
	~Cat();
	Cat &	operator=( Cat const & rhs );
//	Cat &	operator=( Animal const & rhs );

	void	makeSound() const;

	void	printBrain() const;
	void	printBrain(int pos) const;
	Brain *	getBrain() const;

private:

	Brain	*_brain;

};

#endif

