#ifndef DOG_HPP
# define DOG_HPP

# include "color.h"
# include <iostream>
# include <string>

# include "Animal.hpp"
# include "Brain.hpp"

class Dog : public Animal {

public:

	Dog();
	Dog( Brain *brain );
	Dog( Dog const & src );
	~Dog();
	Dog &	operator=( Dog const & rhs );
//	Dog &	operator=( Animal const & rhs );

	void	makeSound() const;
	Brain *	getBrain() const;

private:

	Brain	*_brain;

};

#endif

