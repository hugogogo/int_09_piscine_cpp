#ifndef ANIMAL_HPP
# define ANIMAL_HPP

# include "color.h"
# include <iostream>
# include <string>
# include "Brain.hpp"

class Animal {

public:
	Animal();
	Animal( Animal const & src );
	virtual ~Animal( void );
	virtual Animal &	operator=( Animal const & rhs );

	virtual void	makeSound() const = 0;
	std::string	getType() const;

//	virtual Brain *	getBrain() const = 0;

protected:
	std::string	type;

};

#endif

