#include "Animal.hpp"
#include "Dog.hpp"
#include "Cat.hpp"

#include <iostream>
#include <string>

#include "color.h"
#define N_TEST "9"

int	main() {
	int i = 0;

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] test erlazo :" RESET "\n";
	{
		Animal* i = new Cat("I am catwoman");
		Animal* j = new Cat("I am just a cat");
		
		std::cout << std::endl;
		std::cout << B_BLUE "cat i : " RESET;
		dynamic_cast<Cat*>(i)->getBrain()->printIdea(0);
		std::cout << B_BLUE "cat j : " RESET;
		dynamic_cast<Cat*>(j)->getBrain()->printIdea(0);
		
		std::cout << "\n" B_BLUE "*i = *j" RESET "\n";
		*(dynamic_cast<Cat*>(i)) = *(dynamic_cast<Cat*>(j));
		
		std::cout << B_BLUE "cat i : " RESET;
		dynamic_cast<Cat*>(i)->getBrain()->printIdea(0);
		std::cout << B_BLUE "cat j : " RESET;
		dynamic_cast<Cat*>(j)->getBrain()->printIdea(0);
		
		std::cout << "\n" B_BLUE "j->getBrain->putIdea(\"I am not a cat\")" RESET "\n";
		dynamic_cast<Cat*>(j)->getBrain()->putIdea(0, "I am not a cat");;
		
		std::cout << B_BLUE "cat i : " RESET;
		dynamic_cast<Cat*>(i)->getBrain()->printIdea(0);
		std::cout << B_BLUE "cat j : " RESET;
		dynamic_cast<Cat*>(j)->getBrain()->printIdea(0);
		
		std::cout << std::endl;
		std::cout << B_BLUE "delete i" RESET "\n";
		delete i;
		std::cout << B_BLUE "delete j" RESET "\n";
		delete j;
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] test subject :" RESET "\n";
	{
		const Animal* j = new Dog();
		const Animal* i = new Cat();
		delete j;//should not create a leak
		delete i;
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] test with brain :" RESET "\n";
	{
		Dog *	dog;
		Cat *	cat1;
		Cat		cat2;
		Brain *	brain1 = new Brain();
		Brain *	brain2 = new Brain();

			std::cout << B_BLUE "fill brain1 with \"giraffe\" :" RESET "\n";
		brain1->putIdeas("giraffe");
			std::cout << B_BLUE "print brain1 :" RESET "\n";
		brain1->printIdeas();
			std::cout << B_BLUE "print brain2 :" RESET "\n";
		brain2->printIdeas();
			std::cout << B_BLUE "brain2 copy brain1 :" RESET "\n";
		*brain2 = *brain1;
			std::cout << B_BLUE "fill brain1 with \"hippopotamus\" :" RESET "\n";
		brain1->putIdeas("hippopotamus");
			std::cout << B_BLUE "print brain1 :" RESET "\n";
		brain1->printIdeas();
			std::cout << B_BLUE "print brain2 :" RESET "\n";
		brain2->printIdeas();

			std::cout << B_BLUE "create new dog with brain1 :" RESET "\n";
		dog = new Dog(brain1);
			std::cout << B_BLUE "create new cat with brain1 :" RESET "\n";
		cat1 = new Cat(brain1);
			std::cout << B_BLUE "cat2 copy cat1 :" RESET "\n";
		cat2 = *cat1;

			std::cout << B_BLUE "fill brain1 with \"zebra\" :" RESET "\n";
		brain1->putIdeas("zebra");
			std::cout << B_BLUE "print cat1 :" RESET "\n";
		cat1->printBrain();
			std::cout << B_BLUE "print cat2 :" RESET "\n";
		cat2.printBrain();

			std::cout << B_BLUE "delete dog :" RESET "\n";
		delete dog;
			std::cout << B_BLUE "delete cat1 :" RESET "\n";
		delete cat1;
			std::cout << B_BLUE "delete brain1 :" RESET "\n";
		delete brain1;
			std::cout << B_BLUE "delete brain2 :" RESET "\n";
		delete brain2;
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] test with brain :" RESET "\n";
	{
		Cat *	cat1;
		Cat		cat2;
		Brain *	brain1 = new Brain();


			std::cout << B_BLUE "create new cat with brain1 :" RESET "\n";
		cat1 = new Cat(brain1);
			std::cout << B_BLUE "cat2 copy cat1 :" RESET "\n";
		cat2 = *cat1;

			std::cout << B_BLUE "delete cat1 :" RESET "\n";
		delete cat1;
			std::cout << B_BLUE "delete brain1 :" RESET "\n";
		delete brain1;
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] array animal test :" RESET "\n";
	{
		Animal	*animals[10];
		int		i;

		for (i = 0 ; i < 5 ; ++i)
			animals[i] = new Cat();
		for ( ; i < 10 ; ++i)
			animals[i] = new Dog();
		for (i = 0 ; i < 10 ; ++i)
			animals[i]->makeSound();
		for (i = 0 ; i < 10 ; ++i)
			delete animals[i];
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] copy constructor test1/2 :" RESET "\n";
	{
		std::cout << B_BLUE "Cat a_cat :" RESET "\n";
		Cat	a_cat;
		std::cout << B_BLUE "Cat a_cpy_cat(a_cat) :" RESET "\n";
		Cat	a_cpy_cat(a_cat);
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] copy constructor test2/2 :" RESET "\n";
	{
		std::cout << B_BLUE "Cat a_cat :" RESET "\n";
		Cat	a_cat;
		std::cout << B_BLUE "Cat a_cpy_cat = a_cat :" RESET "\n";
		Cat	a_cpy_cat = a_cat;
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] assignation operator test1 :" RESET "\n";
	{
		std::cout << B_BLUE "Cat a_cat :" RESET "\n";
		Cat	a_cat;
		std::cout << B_BLUE "Cat a_cpy_cat :" RESET "\n";
		Cat	a_cpy_cat;
		std::cout << B_BLUE "a_cpy_cat = a_cat :" RESET "\n";
		a_cpy_cat = a_cat;
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] assignation operator test2 :" RESET "\n";
	{
		std::cout << B_BLUE "const Cat *a_cat :" RESET "\n";
		const Cat *a_cat = new Cat();
		std::cout << B_BLUE "Cat a_cpy_cat :" RESET "\n";
		Cat	a_cpy_cat;
		std::cout << B_BLUE "a_cpy_cat = *a_cat :" RESET "\n";
		a_cpy_cat = *a_cat;

		delete a_cat;
	}

	return 0;
}

/*
#include <vector>
#include <functional>
#include <iostream>

int main()
{
    std::vector<std::function<void()>> tests;
    tests.push_back(
        [](){
            std::cout << "hello test\n";
        }
    );
    tests.push_back(
        [](){
            std::cout << "hello another test.\n";
        }
    );

    int counter = 0;
    for (const auto& test : tests){
        std::cout << ++counter << "/" << tests.size() << " ";
        test(); 
    }
}
*/
