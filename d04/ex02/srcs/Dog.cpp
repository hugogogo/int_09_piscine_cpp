#include "Dog.hpp"

#define COPLIEN_COLOR B_CYAN

/*********************************************
 * CONSTRUCTORS
 *********************************************/

Dog::Dog() {
	std::cout << COPLIEN_COLOR "Dog constructor" RESET "\n";
	type = "dog";
	_brain = new Brain();
	return;
}

Dog::Dog( Brain *brain ) {
	std::cout << COPLIEN_COLOR "Dog constructor" RESET "\n";
	type = "dog";
	_brain = new Brain();
	*_brain = *brain;
	return;
}

/*
error: base class ‘class Animal’ should be explicitly initialized in the copy constructor [-Werror=extra]
Dog::Dog( Dog const & src ) {
^~~
answer : https://stackoverflow.com/questions/43612772/base-class-class-a-should-be-explicitly-initialized-in-the-copy-constructor
*/
Dog::Dog( Dog const & src ) : Animal() {
	std::cout << COPLIEN_COLOR "Dog copy constructor" RESET "\n";
	*this = src;
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

Dog::~Dog() {
	std::cout << COPLIEN_COLOR "Dog destructor" RESET "\n";
	delete _brain;
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

Dog &	Dog::operator=( Dog const & rhs ) {
	Animal::operator=(rhs);
	std::cout << COPLIEN_COLOR "Dog assignator" RESET "\n";
	if (this != &rhs)
		*_brain = *(rhs.getBrain());
	return *this;
}
// need of a second overload in case "Animal cat" = "Animal cat";
// https://stackoverflow.com/questions/68248198/why-my-virtual-assignment-operator-not-doing-as-intended
/*
Dog &	Dog::operator=( Animal const & rhs ) {
	Animal::operator=(rhs);
	std::cout << COPLIEN_COLOR "Cat (Animal) assignator" RESET "\n";
	if (this != &rhs)
		*_brain = *(rhs.getBrain());
	return *this;
}
*/

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	Dog::makeSound() const {
	std::cout << "*woof*\n";
}

Brain *	Dog::getBrain() const { return _brain; }
