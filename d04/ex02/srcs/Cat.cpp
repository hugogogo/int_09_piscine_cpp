#include "Cat.hpp"

#define COPLIEN_COLOR B_CYAN

/*********************************************
 * CONSTRUCTORS
 *********************************************/

/*
 * default arguments in default constructor : https://stackoverflow.com/questions/187640/default-parameters-with-c-constructors
 * in this cas it doesn't work i think, since both constructors don't act exactly the same
 */
Cat::Cat() {
	std::cout << COPLIEN_COLOR "Cat default constructor" RESET "\n";
	type = "cat";
	_brain = new Brain();
	return;
}
Cat::Cat( Brain * brain ) {
	std::cout << COPLIEN_COLOR "Cat parameters brain constructor" RESET "\n";
	type = "cat";
	_brain = new Brain();
	*_brain = *brain;
	return;
}
Cat::Cat( std::string ideas ) {
	std::cout << COPLIEN_COLOR "Cat parameters ideas constructor" RESET "\n";
	type = "cat";
	_brain = new Brain();
	_brain->putIdeas(ideas);
	return;
}

/*
 * error: base class ‘class Animal’ should be explicitly initialized in the copy constructor [-Werror=extra]
 * Cat::Cat( Cat const & src ) {
 * ^~~
 * answer : https://stackoverflow.com/questions/43612772/base-class-class-a-should-be-explicitly-initialized-in-the-copy-constructor
 */
Cat::Cat( Cat const & src ) : Animal(src) {
	std::cout << COPLIEN_COLOR "Cat copy constructor" RESET "\n";
	_brain = new Brain();
	*this = src;
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

Cat::~Cat() {
	std::cout << COPLIEN_COLOR "Cat destructor" RESET "\n";
	delete _brain;
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

Cat &	Cat::operator=( Cat const & rhs ) {
	Animal::operator=(rhs);
	std::cout << COPLIEN_COLOR "Cat assignator" RESET "\n";
	if (this != &rhs)
		*_brain = *(rhs.getBrain());
	return *this;
}
// need of a second overload in case "Animal cat" = "Animal cat";
// https://stackoverflow.com/questions/68248198/why-my-virtual-assignment-operator-not-doing-as-intended
/*
Cat &	Cat::operator=( Animal const & rhs ) {
	Animal::operator=(rhs);
	std::cout << COPLIEN_COLOR "Cat (Animal) assignator" RESET "\n";
	if (this != &rhs)
		*_brain = *(rhs.getBrain());
	return *this;
}
*/

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	Cat::makeSound() const {
	std::cout << "*miaow*\n";
}
void	Cat::printBrain() const {
	_brain->printIdeas();
}
void	Cat::printBrain(int pos) const {
	_brain->printIdea(pos);
}

Brain *	Cat::getBrain() const { return _brain; }

