#ifndef CAT_HPP
# define CAT_HPP

#include "color.h"
#include <iostream>
#include <string>

#include "Animal.hpp"

class Cat : public Animal {

public:

	Cat( void );
	Cat( Cat const & src );
	~Cat( void );
	Cat &	operator=( Cat const & rhs );

	void	makeSound() const;

protected:

private:

};

#endif

