#ifndef ANIMAL_HPP
# define ANIMAL_HPP

# include "color.h"
#include <iostream>
#include <string>

class Animal {

public:

	Animal( void );
	Animal( Animal const & src );
	virtual ~Animal( void );
	Animal &	operator=( Animal const & rhs );

	virtual void	makeSound() const;
	std::string	getType() const;

protected:

	std::string	type;

private:

};

#endif

