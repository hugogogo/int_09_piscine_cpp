#include "Dog.hpp"

#define COPLIEN_COLOR B_CYAN

/*********************************************
 * CONSTRUCTORS
 *********************************************/

Dog::Dog() {
	std::cout << COPLIEN_COLOR "Dog constructor" RESET "\n";
	type = "dog";
	return;
}

Dog::Dog( Dog const & src ) : Animal(src) {
	std::cout << COPLIEN_COLOR "Dog copy constructor" RESET "\n";
	*this = src;
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

Dog::~Dog() {
	std::cout << COPLIEN_COLOR "Dog destructor" RESET "\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

Dog &	Dog::operator=( Dog const & rhs ) {
	Animal::operator=(rhs);
	return *this;
}

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	Dog::makeSound() const {
	std::cout << "*woof*\n";
}

