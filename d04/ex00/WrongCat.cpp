#include "WrongCat.hpp"

#define COPLIEN_COLOR B_CYAN

/*********************************************
 * CONSTRUCTORS
 *********************************************/

WrongCat::WrongCat() {
	std::cout << COPLIEN_COLOR "WrongCat constructor" RESET "\n";
	type = "wrong_cat";
	return;
}

WrongCat::WrongCat( WrongCat const & src ) : WrongAnimal(src) {
	std::cout << COPLIEN_COLOR "WrongCat copy constructor" RESET "\n";
	*this = src;
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

WrongCat::~WrongCat() {
	std::cout << COPLIEN_COLOR "Cat destructor" RESET "\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

WrongCat &	WrongCat::operator=( WrongCat const & rhs ) {
	WrongAnimal::operator=(rhs);
	return *this;
}

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	WrongCat::makeSound() const {
	std::cout << "*miaow*\n";
}
