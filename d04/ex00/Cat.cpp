#include "Cat.hpp"

#define COPLIEN_COLOR B_CYAN

/*********************************************
 * CONSTRUCTORS
 *********************************************/

Cat::Cat() {
	std::cout << COPLIEN_COLOR "Cat constructor" RESET "\n";
	type = "cat";
	return;
}

Cat::Cat( Cat const & src ) : Animal(src) {
	std::cout << COPLIEN_COLOR "Cat copy constructor" RESET "\n";
	*this = src;
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

Cat::~Cat() {
	std::cout << COPLIEN_COLOR "Cat destructor" RESET "\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

Cat &	Cat::operator=( Cat const & rhs ) {
	Animal::operator=(rhs);

	return *this;
}

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	Cat::makeSound() const {
	std::cout << "*miaow*\n";
}

