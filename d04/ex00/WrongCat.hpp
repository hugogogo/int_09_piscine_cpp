#ifndef WRONG_CAT_HPP
# define WRONG_CAT_HPP

#include "color.h"
#include <iostream>
#include <string>

#include "WrongAnimal.hpp"

class WrongCat : public WrongAnimal {

public:

	WrongCat( void );
	WrongCat( WrongCat const & src );
	~WrongCat( void );
	WrongCat &	operator=( WrongCat const & rhs );

	void	makeSound() const;

};

#endif

