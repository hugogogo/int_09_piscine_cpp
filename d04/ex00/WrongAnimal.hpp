#ifndef WRONG_ANIMAL_HPP
# define WRONG_ANIMAL_HPP

#include "color.h"
#include <iostream>
#include <string>

class WrongAnimal {

public:

	WrongAnimal( void );
	WrongAnimal( WrongAnimal const & src );
	virtual ~WrongAnimal( void );
	WrongAnimal &	operator=( WrongAnimal const & rhs );

	void	makeSound() const;
	std::string	getType() const;

protected:

	std::string	type;

};

#endif

