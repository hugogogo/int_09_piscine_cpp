#include "Animal.hpp"

#define COPLIEN_COLOR B_CYAN

/*********************************************
 * CONSTRUCTORS
 *********************************************/

Animal::Animal() {
	std::cout << COPLIEN_COLOR "Animal constructor" RESET "\n";
	type = "animal";
	return;
}

Animal::Animal( Animal const & src ) {
	std::cout << COPLIEN_COLOR "Animal copy constructor" RESET "\n";
	*this = src;
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

Animal::~Animal() {
	std::cout << COPLIEN_COLOR "Animal destructor" RESET "\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

Animal &	Animal::operator=( Animal const & rhs ) {
	if ( this != &rhs )
	{
		type = rhs.getType();
	}
	return *this;
}

std::string	Animal::getType() const {return type;}

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	Animal::makeSound() const {
	std::cout << "*sound*\n";
}

