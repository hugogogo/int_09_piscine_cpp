#include "WrongAnimal.hpp"

#define COPLIEN_COLOR B_CYAN

/*********************************************
 * CONSTRUCTORS
 *********************************************/

WrongAnimal::WrongAnimal() {
	std::cout << COPLIEN_COLOR "WrongAnimal constructor" RESET "\n";
	type = "wrong_animal";
	return;
}

WrongAnimal::WrongAnimal( WrongAnimal const & src ) {
	std::cout << COPLIEN_COLOR "WrongAnimal copy constructor" RESET "\n";
	*this = src;
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

WrongAnimal::~WrongAnimal() {
	std::cout << COPLIEN_COLOR "WrongAnimal destructor" RESET "\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

WrongAnimal &	WrongAnimal::operator=( WrongAnimal const & rhs ) {
	if ( this != &rhs )
	{
		type = rhs.getType();
	}
	return *this;
}

/*********************************************
 * ACCESSORS
 *********************************************/

std::string	WrongAnimal::getType() const {return type;}

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	WrongAnimal::makeSound() const {
	std::cout << "*sound*\n";
}

