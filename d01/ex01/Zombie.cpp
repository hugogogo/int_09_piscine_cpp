#include "Zombie.hpp"

Zombie::Zombie() {

	std::cout << this->_name << " has been created" << std::endl;

}

Zombie::~Zombie() {

	std::cout << this->_name << " has been destroyed" << std::endl;

}

void Zombie::announce( void ) {

	std::cout << this->_name << ": BraiiiiiiinnnzzzZ..." << std::endl;

}

void Zombie::put_name( std::string name ) {

	this->_name = name;

}

