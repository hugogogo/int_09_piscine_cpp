#include "Zombie.hpp"

Zombie* zombieHorde( int N, std::string name );
#define z_nb 8

int	main(void)
{
	Zombie*	zombi;

	zombi = zombieHorde( z_nb, "rambou" );
	for (int i = 0; i < z_nb; i++) {
		zombi[i].announce();
	}
	delete [] zombi;

	return (0);
}
