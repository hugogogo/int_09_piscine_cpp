#ifndef ZOMBIE_HPP
# define ZOMBIE_HPP

#include <iostream>
#include <string>

class Zombie {

public:

	Zombie();
	~Zombie();

	void announce( void );
	void put_name( std::string name );

private:

	std::string _name;

};

#endif
