#include "Karen.hpp"


Karen::Karen() {

	_fp[0] = &Karen::_debug;
	_fp[1] = &Karen::_info;
	_fp[2] = &Karen::_warning;
	_fp[3] = &Karen::_error;

	_level[0] = "DEBUG";
	_level[1] = "INFO";
	_level[2] = "WARNING";
	_level[3] = "ERROR";

	return;

}
Karen::~Karen() {return;}

void Karen::complain( std::string level ) {

	int	size;

	size = sizeof(this->_level) / sizeof(this->_level[0]);

	for (int i = 0; i < size; i++)
	{
		if (level.compare(this->_level[i]) == 0)
			(this->*_fp[i])();
	}

}

void Karen::_debug( void )		{std::cout << "debug"   << '\n';}
void Karen::_info( void )		{std::cout << "info"    << '\n';}
void Karen::_warning( void )	{std::cout << "warning" << '\n';}
void Karen::_error( void )		{std::cout << "error"   << '\n';}
