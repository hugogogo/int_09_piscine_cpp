#include "Karen.hpp"
#include <iostream>
#include <string>

int	main(int ac, char *av[])
{
	Karen		karen;
	std::string	level[4] = { "DEBUG", "INFO", "WARNING", "ERROR" };
	int			i;

	if (ac != 2)
	{
		std::cout << "need 1 argument of type DEBUG, INFO, WARNING or ERROR\n";
		return (1);
	}

	for (i = 0; i < 4; i++)
		if (level[i].compare(av[1]) == 0)
			break;

	switch (i)
	{
		case (0):
			std::cout << "[ DEBUG ]" << '\n';
			karen.complain("DEBUG");
			std::cout << '\n';
			/* FALLTHRU */
		case (1):
			std::cout << "[ INFO ]" << '\n';
			karen.complain("INFO");
			std::cout << '\n';
			/* FALLTHRU */
		case (2):
			std::cout << "[ WARNING ]" << '\n';
			karen.complain("WARNING");
			std::cout << '\n';
			/* FALLTHRU */
		case (3):
			std::cout << "[ ERROR ]" << '\n';
			karen.complain("ERROR");
			break ;
		default:
			std::cout << "[ Probably complaining about insignificant problems ]" << '\n';
	}

	return (0);
}
