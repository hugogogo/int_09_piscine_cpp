#include "HumanA.hpp"

HumanA::HumanA( std::string name, Weapon &weapon ) : _weapon( weapon ) {

	this->_name = name;
//	this->_weapon = weapon;

	return;

}
HumanA::~HumanA() {return;}

void HumanA::attack( void ) const {

	std::cout << this->_name << " attacks with their ";
	std::cout << this->_weapon.getType() << std::endl;

	return;

}
