#include "HumanB.hpp"

HumanB::HumanB( std::string name ) {
	this->_name = name;
}
HumanB::~HumanB() {}

void HumanB::setWeapon( Weapon &weapon ) {
	this->_weapon = &weapon;
}

void HumanB::attack( void ) {
	if (!this->_weapon)
		return;
	std::cout << this->_name << " attacks with their ";
	std::cout << this->_weapon->getType() << std::endl;
}

