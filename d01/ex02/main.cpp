#include <iostream>
#include <string>

int	main(void)
{
	std::string brain = "HI THIS IS BRAIN";
	std::string * stringPTR = &brain;
	std::string & stringREF = brain;

//	• L’adresse de la string en mémoire.
//	• L’adresse stockée dans stringPTR.
//	• L’adresse stockée dans stringREF.
//	Puis :
//	• La valeur de la string.
//	• La valeur pointée par stringPTR.
//	• La valeur pointée par stringREF.

	std::cout << &brain << std::endl;
	std::cout << stringPTR << std::endl;
	std::cout << &stringREF << std::endl << std::endl;
	std::cout << brain << std::endl;
	std::cout << *stringPTR << std::endl;
	std::cout << stringREF << std::endl;

	return (0);
}
