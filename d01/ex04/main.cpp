#include "Sed.hpp"

#include <iostream>
#include <string>

int	sed_error(std::string msg) {

	std::cout << msg << std::endl;
	return 1;
}

void	cpp_sed(char **av) {

	Sed	sed(av[1], av[2], av[3]);
	
	sed.replace();

}

int	main(int ac, char **av) {

	if (ac != 4)
		return sed_error("you must provide 3 parameters : <file> <string 1> <string 2>");
	cpp_sed(av);

	return 0;

}
