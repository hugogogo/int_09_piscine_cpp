#!/bin/bash
cd $(dirname $0)
source ./colors.sh
mkdir -p files_log
make -C ../ &> /dev/null

EXECUTABLE="sed"
FILES_LOG="files_log"
LEAKS=""
if [ $1 == "leaks" ] || [ $1 == "valgrind" ]
then
	LEAKS="valgrind --leak-check=full --show-leak-kinds=all"
fi

# RUN TESTS
	function run_tests
	{
		if [ -f $TESTNAME ];then
			echo "$CONTENT" > $TESTNAME;fi

		$LEAKS ../$EXECUTABLE $TESTNAME "$FIND" "$REPLACEMENT"
		OUTPUT=$( cat $TESTNAME.replace )
		if [ "$OUTPUT" == "$RESULT" ]
		then
			echo -e $B_GREEN"$TESTNAME success"$ENDCO
		else
			echo -e $B_RED"$TESTNAME faillure"$ENDCO
		fi

		if [ -f $TESTNAME ];then
			mv $TESTNAME ./$FILES_LOG;fi
		if [ -f $TESTNAME.replace ];then
			mv $TESTNAME.replace ./$FILES_LOG;fi
	}

# TEST 1 ########################################
TESTNAME="one_line_file"
touch $TESTNAME
FIND=" "
REPLACEMENT="hello"
CONTENT=" "
RESULT="hello"
run_tests

# TEST 2 ########################################
TESTNAME="multi_line_file"
touch $TESTNAME
FIND="ie"
REPLACEMENT="++"
CONTENT=$(cat << EOF
ce fichier
contient
plusieurs lignes
les unes au dessus des autres
youhouuu ioieux
EOF
)
RESULT=$(cat << EOF
ce fich++r
cont++nt
plus++urs lignes
les unes au dessus des autres
youhouuu io++ux
EOF
)
run_tests

# TEST 3 ########################################
TESTNAME="replace_one_character"
touch $TESTNAME
FIND="."
REPLACEMENT="+"
CONTENT=$(cat << EOF
....................................................;
EOF
)
RESULT=$(cat << EOF
++++++++++++++++++++++++++++++++++++++++++++++++++++;
EOF
)
run_tests

# TEST 4 ########################################
TESTNAME="replace_two_characters"
touch $TESTNAME
FIND=".."
REPLACEMENT="++"
CONTENT=$(cat << EOF
...................................................;
. . . . . . . . . . . . . . . . . . . . . . . . . .;
EOF
)
RESULT=$(cat << EOF
++++++++++++++++++++++++++++++++++++++++++++++++++.;
. . . . . . . . . . . . . . . . . . . . . . . . . .;
EOF
)
run_tests

# TEST 5 ########################################
TESTNAME="first_letter_lure"
touch $TESTNAME
FIND="mdr"
REPLACEMENT="|||"
CONTENT=$(cat << EOF
test de mmdr foubar
EOF
)
RESULT=$(cat << EOF
test de m||| foubar
EOF
)
run_tests

# TEST 6 ########################################
TESTNAME="half_replacement_lure"
touch $TESTNAME
FIND="toutouille"
REPLACEMENT="||||||||||"
CONTENT=$(cat << EOF
trouve ce mot toutouille
et celui-la toutoutouille
EOF
)
RESULT=$(cat << EOF
trouve ce mot ||||||||||
et celui-la tou||||||||||
EOF
)
run_tests

# TEST 7 ########################################
TESTNAME="special_character_'"
touch $TESTNAME
FIND="n't"
REPLACEMENT="000"
CONTENT=$(cat << EOF
No more tears, my heart is dry
I don't laugh and I don't cry
I don't think about you all the time
But when I do - I wonder why

You have to go out of my door
And leave just like you did before
I know I said that I was sure
But rich men can't imagine poor.

One day baby, we'll be old
Oh baby, we'll be old
And think of all the stories that we could have told

Little me and little you
Kept doing all the things they do
They never really think it through
Like I can never think you're true

Here I go again - the blame
The guilt, the pain, the hurt, the shame
The founding fathers of our plane
That's stuck in heavy clouds of rain.

One day baby, we'll be old
Oh baby, we'll be old
And think of all the stories that we could have told
EOF
)
RESULT=$(cat << EOF
No more tears, my heart is dry
I do000 laugh and I do000 cry
I do000 think about you all the time
But when I do - I wonder why

You have to go out of my door
And leave just like you did before
I know I said that I was sure
But rich men ca000 imagine poor.

One day baby, we'll be old
Oh baby, we'll be old
And think of all the stories that we could have told

Little me and little you
Kept doing all the things they do
They never really think it through
Like I can never think you're true

Here I go again - the blame
The guilt, the pain, the hurt, the shame
The founding fathers of our plane
That's stuck in heavy clouds of rain.

One day baby, we'll be old
Oh baby, we'll be old
And think of all the stories that we could have told
EOF
)
run_tests

# TEST 8 ########################################
TESTNAME="multiline_replacement"
touch $TESTNAME
FIND=$(cat << EOF
ry
I don't 
EOF
)
REPLACEMENT=$(cat << EOF
oo
oooooooo
EOF
)
CONTENT=$(cat << EOF
No more tears, my heart is dry
I don't laugh and I don't cry
I don't think about you all the time
But when I do - I wonder why
EOF
)
RESULT=$(cat << EOF
No more tears, my heart is doo
oooooooolaugh and I don't coo
oooooooothink about you all the time
But when I do - I wonder why
EOF
)
run_tests

# TEST 9 ########################################
TESTNAME="empty_find"
touch $TESTNAME
FIND=""
REPLACEMENT="p"
CONTENT=$(cat << EOF
No more tears, my heart is dry
I don't laugh and I don't cry
I don't think about you all the time
But when I do - I wonder why
EOF
)
RESULT=$(cat << EOF
No more tears, my heart is dry
I don't laugh and I don't cry
I don't think about you all the time
But when I do - I wonder why
EOF
)
run_tests

# TEST 10 ########################################
TESTNAME="empty_find_and_replacement"
touch $TESTNAME
FIND=""
REPLACEMENT=""
CONTENT=$(cat << EOF
No more tears, my heart is dry
I don't laugh and I don't cry
I don't think about you all the time
But when I do - I wonder why
EOF
)
RESULT=$(cat << EOF
No more tears, my heart is dry
I don't laugh and I don't cry
I don't think about you all the time
But when I do - I wonder why
EOF
)
run_tests

# TEST 11 ########################################
TESTNAME="empty_replacement"
touch $TESTNAME
FIND="a"
REPLACEMENT=""
CONTENT=$(cat << EOF
No more tears, my heart is dry
I don't laugh and I don't cry
I don't think about you all the time
But when I do - I wonder why
EOF
)
RESULT=$(cat << EOF
No more ters, my hert is dry
I don't lugh nd I don't cry
I don't think bout you ll the time
But when I do - I wonder why
EOF
)
run_tests

# TEST 12 ########################################
TESTNAME="file_does_not_exist"
FIND="o"
REPLACEMENT="O"
CONTENT=$(cat << EOF
No more tears, my heart is dry
I don't laugh and I don't cry
I don't think about you all the time
But when I do - I wonder why
EOF
)
RESULT=""
run_tests
