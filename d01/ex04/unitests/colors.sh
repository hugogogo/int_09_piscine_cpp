# COLORS
	RED="\e[0;31m"
	GREEN="\e[0;32m"
	YELLOW="\e[0;33m"
	BLUE="\e[0;34m"
	MAGENTA="\e[0;35m"
	CYAN="\e[0;36m"
	WHITE="\e[0;37m"
	
	B_RED="\e[1;31m"
	B_GREEN="\e[1;32m"
	B_YELLOW="\e[1;33m"
	B_BLUE="\e[1;34m"
	B_MAGENTA="\e[1;35m"
	B_CYAN="\e[1;36m"
	B_WHITE="\e[1;37m"
	
	ENDCO="\e[0m"

