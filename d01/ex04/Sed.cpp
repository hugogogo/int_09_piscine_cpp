#include "Sed.hpp"

Sed::Sed(char *file, char *find, char *replacement)
	: _file( file )
	, _new_file( std::string(file).append(".replace") )
	, _find( find )
	, _replacement( replacement ) {

	return;
}
Sed::~Sed() {return;}

void	Sed::replace() {

	std::ifstream	file(this->_file.c_str());
	std::ofstream	new_file(this->_new_file.c_str());
	int				len = this->_find.length();
	std::string		str;
	char			cstr[len + 1];

	if (file.fail() || new_file.fail())
		return;

	if (len)
		file.get(cstr, len + 1, EOF);
	else
		file.get(cstr, len + 2, EOF);
	str.assign(cstr);
	while (!file.eof())
	{
		if (len && this->_find.compare(str) == 0)
		{
			new_file << this->_replacement;
			file.get(cstr, len + 1, EOF);
			str.assign(cstr);
			continue;
		}
		else
			new_file << str[0];
		str.erase(str.begin());
		str.push_back(file.get());
	}
	str.erase(str.end() - 1);
	new_file << str;
}
