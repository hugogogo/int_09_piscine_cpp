#ifndef SED_HPP
# define SED_HPP

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

class Sed {

public:

	 Sed(char *file, char *find, char *replacement);
	~Sed();

	void replace( void );

private:

//	std::string			_buf;
	std::string			_tmp;
	std::string	const	_file;
	std::string	const	_new_file;
	std::string const	_find;
	std::string const	_replacement;

};

#endif
