#ifndef EASYFIND_HPP
# define EASYFIND_HPP

# include "colors.h"
# include <algorithm>

class easyfindException : public std::exception {
	virtual char const *what(void) const throw() {
		return B_RED "not found" RESET;
	}
};

template < typename T >
typename T::const_iterator	easyfind(T const & container, int nb) {
	typename T::const_iterator	it;

	it = std::find(container.begin(), container.end(), nb);
	if (it == container.end())
		throw easyfindException();
	return it;
}

#endif

