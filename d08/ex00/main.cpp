#include <iostream>
#include <string>
#include "colors.h"

#include "easyfind.hpp"
#include <list>
#include <vector>
#include <deque>

#define N_TEST "3"

template < typename T >
void	standardTest(T container, int start, int end, int test) {
	typename T::const_iterator	it;

	for (int i = start ; i < end ; i++)
		container.push_back(i);
	for (it = container.begin(); it != container.end(); it++)
		std::cout << *it << ": " << &*it << "\n";
	std::cout << "\n";
	try {
		it = easyfind(container, test);
		std::cout << *it << ": " << &*it << "\n\n";
	}
	catch (std::exception const & e) {
		std::cout << test << ": " << e.what() << "\n\n";
	}
}

int	main() {
    int i = 0;

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "tests list :" RESET "\n";
    {
		std::cout << B_BLUE "char A - L; find H :" RESET "\n";
		std::list<char>	container;
		standardTest(container, 'A', 'L', 'H');

		std::cout << B_BLUE "char A - L; find L :" RESET "\n";
		std::list<char>	container1;
		standardTest(container1, 'A', 'L', 'L');

		std::cout << B_BLUE "int -5 - 5; find 4 :" RESET "\n";
		std::list<int>	container2;
		standardTest(container2, -5, 5, 4);

		std::cout << B_BLUE "int -5 - 5; find 5 :" RESET "\n";
		std::list<int>	container3;
		standardTest(container3, -5, 5, 5);

		std::cout << B_BLUE "int -5 - 5; find 6 :" RESET "\n";
		std::list<int>	container4;
		standardTest(container4, -5, 5, 6);
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "tests vector :" RESET "\n";
    {
		std::cout << B_BLUE "char A - L; find H :" RESET "\n";
		std::vector<char>	container;
		standardTest(container, 'A', 'L', 'H');

		std::cout << B_BLUE "char A - L; find L :" RESET "\n";
		std::vector<char>	container1;
		standardTest(container1, 'A', 'L', 'L');

		std::cout << B_BLUE "int -5 - 5; find 4 :" RESET "\n";
		std::vector<int>	container2;
		standardTest(container2, -5, 5, 4);

		std::cout << B_BLUE "int -5 - 5; find 5 :" RESET "\n";
		std::vector<int>	container3;
		standardTest(container3, -5, 5, 5);

		std::cout << B_BLUE "int -5 - 5; find 6 :" RESET "\n";
		std::vector<int>	container4;
		standardTest(container4, -5, 5, 6);
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "tests deque :" RESET "\n";
    {
		std::cout << B_BLUE "char A - L; find H :" RESET "\n";
		std::deque<char>	container;
		standardTest(container, 'A', 'L', 'H');

		std::cout << B_BLUE "char A - L; find L :" RESET "\n";
		std::deque<char>	container1;
		standardTest(container1, 'A', 'L', 'L');

		std::cout << B_BLUE "int -5 - 5; find 4 :" RESET "\n";
		std::deque<int>	container2;
		standardTest(container2, -5, 5, 4);

		std::cout << B_BLUE "int -5 - 5; find 5 :" RESET "\n";
		std::deque<int>	container3;
		standardTest(container3, -5, 5, 5);

		std::cout << B_BLUE "int -5 - 5; find 6 :" RESET "\n";
		std::deque<int>	container4;
		standardTest(container4, -5, 5, 6);
	}

	return 0;
}

