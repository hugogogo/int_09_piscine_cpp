#ifndef MUTANTSTACK_HPP
# define MUTANTSTACK_HPP

#include <stack>

template <typename T>
class MutantStack : public std::stack<T> {

public:
	/*
	typedef T * iterator;
	typedef T const * const_iterator;

	iterator		begin() 		{return (&this->top() - (this->size() - 1));}
	iterator 		end()			{return (&this->top() + 1);}

	const_iterator	begin() const	{return (&this->top() - (this->size() - 1));}
	const_iterator	end() const		{return (&this->top() + 1);}
	*/

	typedef typename std::stack<T>::container_type::iterator		iterator;
	typedef typename std::stack<T>::container_type::const_iterator	const_iterator;

	iterator		begin() 		{return this->c.begin();}
	iterator 		end()			{return this->c.end();}
	const_iterator	begin() const	{return this->c.begin();}
	const_iterator	end() const		{return this->c.end();}
};

#endif

