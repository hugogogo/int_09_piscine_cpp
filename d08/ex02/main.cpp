#include <iostream>
#include "colors.h"
#include "MutantStack.hpp"
#include <cstdlib> // rand

#define N_TEST "3"

int	main() {
	int i = 0;
	srand(time(NULL));

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "test simple iterator :" RESET "\n";
    {
		MutantStack<int> mstack;

		mstack.push(7);
		mstack.push(987);
		mstack.push(9);
		mstack.push(8);
		mstack.push(34);
		mstack.push(1);

		MutantStack<int>::iterator it = mstack.begin();
		MutantStack<int>::iterator ite = mstack.end();

		for (; it != ite; it++)
			std::cout << *it << "\n";
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "test stack size 128:" RESET "\n";
    {
		MutantStack<int> mstack;

		mstack.push(-42);
		for (unsigned int i = 0; i < 126; i++)
			mstack.push(rand() % 10000000000000);
		mstack.push(42);

		MutantStack<int>::iterator it = mstack.begin();
		MutantStack<int>::iterator ite = mstack.end();

		std::cout << "[ ";
		for (; it != ite; it++)
			std::cout << *it << " ";
		std::cout << "]\n";

		it = mstack.begin();
		ite = mstack.end();

		std::cout << B_BLUE "firtst and last :" RESET "\n";
		std::cout << *it << " | " << *(ite - 1) << "\n\n";
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "test stack size 129:" RESET "\n";
    {
		MutantStack<int> mstack;

		mstack.push(-42);
		for (unsigned int i = 0; i < 127; i++)
			mstack.push(rand() % 10000000000000);
		mstack.push(42);

		MutantStack<int>::iterator it = mstack.begin();
		MutantStack<int>::iterator ite = mstack.end();

		std::cout << "[ ";
		for (; it != ite; it++)
			std::cout << *it << " ";
		std::cout << "]\n";

		it = mstack.begin();
		ite = mstack.end();

		std::cout << B_BLUE "firtst and last :" RESET "\n";
		std::cout << *it << " | " << *(ite - 1) << "\n\n";
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "test stack size 10000:" RESET "\n";
    {
		MutantStack<int> mstack;

		mstack.push(-42);
		for (unsigned int i = 0; i < 10000; i++)
			mstack.push(rand() % 10000000000000);
		mstack.push(42);

		MutantStack<int>::iterator it = mstack.begin();
		MutantStack<int>::iterator ite = mstack.end();

		std::cout << "[ ";
		for (; it != ite; it++)
			std::cout << *it << " ";
		std::cout << "]\n";

		it = mstack.begin();
		ite = mstack.end();

		std::cout << B_BLUE "firtst and last :" RESET "\n";
		std::cout << *it << " | " << *(ite - 1) << "\n\n";
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "test const iterator :" RESET "\n";
    {
		MutantStack<int> mstack;

		mstack.push(7);
		mstack.push(987);
		mstack.push(9);
		mstack.push(8);
		mstack.push(34);
		mstack.push(1);

		MutantStack<int>::const_iterator it = mstack.begin();
		MutantStack<int>::const_iterator ite = mstack.end();

		for (; it != ite; it++)
			std::cout << *it << "\n";
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "tests subject :" RESET "\n";
    {
		MutantStack<int> mstack;

		mstack.push(5);
		mstack.push(17);

		std::cout << mstack.top() << std::endl;
		mstack.pop();
		std::cout << mstack.size() << std::endl;

		mstack.push(3);
		mstack.push(5);
		mstack.push(737);

		mstack.push(0);
		MutantStack<int>::iterator it = mstack.begin();
		MutantStack<int>::iterator ite = mstack.end();

		++it;
		--it;

		while (it != ite)
		{
			std::cout << *it << std::endl;
			++it;
		}
		std::stack<int> s(mstack);
	}

	return 0;
}

