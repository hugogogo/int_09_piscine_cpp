#ifndef SPAN_HPP
# define SPAN_HPP

# include "colors.h"
# include <iostream>
# include <string>
# include <vector>
# include <iterator>
# include <algorithm>

class Span {

public:
	Span(unsigned int N);
	Span( Span const & src );
	~Span();
	Span &	operator=( Span const & rhs );

	void	addNumber(int nb);
	template <class InputIterator>
	void    addNumber(InputIterator first, InputIterator last);
//	void    addNumber(int * arr, unsigned int size);

	unsigned int	shortestSpan();
	unsigned int	longestSpan();

//	unsigned int const	getMax() const;

	std::vector<int>::const_iterator	begin() const;
	std::vector<int>::const_iterator	end() const;
	bool								empty() const;


private:
	Span();
	unsigned int const	_max;
	std::vector<int>	_container;
	std::vector<int>	_sort;

};

std::ostream &	operator<<(std::ostream & o, Span const & rhs);

#endif

