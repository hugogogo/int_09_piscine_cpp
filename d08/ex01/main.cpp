#include <iostream>
#include <string>
#include "colors.h"
#include <cstdlib> // rand

#include "Span.hpp"

#define N_TEST "6"

int	main() {
    int i = 0;
	srand(time(NULL));

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "test 10 000 :" RESET "\n";
	{
		unsigned int	len = 10000;
		Span			sp = Span(len);
		int				arr[len];

		for (unsigned int i = 0; i < len; i++)
			arr[i] = rand() % (10000000000000);

		//sp.addNumber(arr, len);
		sp.addNumber(arr, &arr[len]);

		std::cout << B_BLUE "sp :" RESET "\n";
		std::cout << sp << "\n";
		std::cout << sp.shortestSpan() << "\n";
		std::cout << sp.longestSpan() << "\n";
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "test 50 000 :" RESET "\n";
	{
		unsigned int	len = 50000;
		Span			sp = Span(len);
		int				arr[len];

		for (unsigned int i = 0; i < len; i++)
		{
			arr[i] = rand() % (1000000000000000000);
			if (arr[i] % 2)
				arr[i] *= -1;
		}

		//sp.addNumber(arr, len);
		sp.addNumber(arr, &arr[len]);

		std::cout << B_BLUE "sp :" RESET "\n";
		std::cout << sp << "\n";
		std::cout << sp.shortestSpan() << "\n";
		std::cout << sp.longestSpan() << "\n";
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "test copy :" RESET "\n";
    {
		Span sp = Span(5);
		Span sp2 = sp;
		Span sp3 = Span(4);

		sp.addNumber(6);
		sp.addNumber(3);
		sp.addNumber(17);
		sp.addNumber(9);
		sp.addNumber(11);

		sp3.addNumber(1);
		sp3.addNumber(2);
		sp3.addNumber(33);
		sp3.addNumber(4);

		Span sp4 = Span(sp);

		std::cout << B_BLUE "initial" RESET "\n";
		std::cout << "sp : " << sp << "\n";
		std::cout << "sp2: " << sp2 << "\n";
		std::cout << "sp3: " << sp3 << "\n";
		std::cout << "sp4: " << sp4 << "\n";

		sp2 = sp;

		std::cout << B_BLUE "sp2 = sp" RESET "\n";
		std::cout << "sp : " << sp << "\n";
		std::cout << "sp2: " << sp2 << "\n";
		std::cout << "sp3: " << sp3 << "\n";
		std::cout << "sp4: " << sp4 << "\n";

		sp2 = sp3;

		std::cout << B_BLUE "sp2 = sp3" RESET "\n";
		std::cout << "sp : " << sp << "\n";
		std::cout << "sp2: " << sp2 << "\n";
		std::cout << "sp3: " << sp3 << "\n";
		std::cout << "sp4: " << sp4 << "\n";

		sp3 = sp;

		std::cout << B_BLUE "sp3 = sp" RESET "\n";
		std::cout << "sp : " << sp << "\n";
		std::cout << "sp2: " << sp2 << "\n";
		std::cout << "sp3: " << sp3 << "\n";
		std::cout << "sp4: " << sp4 << "\n";

		sp4 = Span(sp2);

		std::cout << B_BLUE "sp4 = Span(sp2)" RESET "\n";
		std::cout << "sp : " << sp << "\n";
		std::cout << "sp2: " << sp2 << "\n";
		std::cout << "sp3: " << sp3 << "\n";
		std::cout << "sp4: " << sp4 << "\n";
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "test out of range exception :" RESET "\n";
    {
		Span sp = Span(5);
		int	nb;
		
		std::cout << B_BLUE "Span sp = Span(5)" RESET "\n";
		std::cout << sp << "\n";
		try {std::cout << B_BLUE "shortestSpan(): " RESET << sp.shortestSpan() << "\n";}
		catch (std::exception &e) {std::cout << e.what() << '\n';}
		try {std::cout << B_BLUE "longestSpan() : " RESET << sp.longestSpan() << "\n";}
		catch (std::exception &e) {std::cout << e.what() << '\n';}
		
		nb = 65;
		try {std::cout << B_BLUE "addNb(" << nb << "): " RESET; sp.addNumber(nb);}
		catch (std::exception &e) {std::cout << e.what() << " ";}
		std::cout << sp << "\n";
		try {std::cout << B_BLUE "shortest(): " RESET << sp.shortestSpan() << "\n";}
		catch (std::exception &e) {std::cout << e.what() << '\n';}
		try {std::cout << B_BLUE "longest() : " RESET << sp.longestSpan() << "\n";}
		catch (std::exception &e) {std::cout << e.what() << '\n';}
		
		nb = 14;
		try {std::cout << B_BLUE "addNb(" << nb << "): " RESET; sp.addNumber(nb);}
		catch (std::exception &e) {std::cout << e.what() << " ";}
		std::cout << sp << "\n";
		try {std::cout << B_BLUE "shortest(): " RESET << sp.shortestSpan() << "\n";}
		catch (std::exception &e) {std::cout << e.what() << '\n';}
		try {std::cout << B_BLUE "longest() : " RESET << sp.longestSpan() << "\n";}
		catch (std::exception &e) {std::cout << e.what() << '\n';}
		
		nb = 28;
		try {std::cout << B_BLUE "addNb(" << nb << "): " RESET; sp.addNumber(nb);}
		catch (std::exception &e) {std::cout << e.what() << " ";}
		std::cout << sp << "\n";
		try {std::cout << B_BLUE "shortest(): " RESET << sp.shortestSpan() << "\n";}
		catch (std::exception &e) {std::cout << e.what() << '\n';}
		try {std::cout << B_BLUE "longest() : " RESET << sp.longestSpan() << "\n";}
		catch (std::exception &e) {std::cout << e.what() << '\n';}
		
		nb = 67;
		try {std::cout << B_BLUE "addNb(" << nb << "): " RESET; sp.addNumber(nb);}
		catch (std::exception &e) {std::cout << e.what() << " ";}
		std::cout << sp << "\n";
		try {std::cout << B_BLUE "shortest(): " RESET << sp.shortestSpan() << "\n";}
		catch (std::exception &e) {std::cout << e.what() << '\n';}
		try {std::cout << B_BLUE "longest() : " RESET << sp.longestSpan() << "\n";}
		catch (std::exception &e) {std::cout << e.what() << '\n';}
		
		nb = 35;
		try {std::cout << B_BLUE "addNb(" << nb << "): " RESET; sp.addNumber(nb);}
		catch (std::exception &e) {std::cout << e.what() << " ";}
		std::cout << sp << "\n";
		try {std::cout << B_BLUE "shortest(): " RESET << sp.shortestSpan() << "\n";}
		catch (std::exception &e) {std::cout << e.what() << '\n';}
		try {std::cout << B_BLUE "longest() : " RESET << sp.longestSpan() << "\n";}
		catch (std::exception &e) {std::cout << e.what() << '\n';}
		
		nb = 82;
		try {std::cout << B_BLUE "addNb(" << nb << "): " RESET; sp.addNumber(nb);}
		catch (std::exception &e) {std::cout << e.what() << " ";}
		std::cout << sp << "\n";
		try {std::cout << B_BLUE "shortest(): " RESET << sp.shortestSpan() << "\n";}
		catch (std::exception &e) {std::cout << e.what() << '\n';}
		try {std::cout << B_BLUE "longest() : " RESET << sp.longestSpan() << "\n";}
		catch (std::exception &e) {std::cout << e.what() << '\n';}
		
		nb = 33;
		try {std::cout << B_BLUE "addNb(" << nb << "): " RESET; sp.addNumber(nb);}
		catch (std::exception &e) {std::cout << e.what() << " ";}
		std::cout << sp << "\n";
		try {std::cout << B_BLUE "shortest(): " RESET << sp.shortestSpan() << "\n";}
		catch (std::exception &e) {std::cout << e.what() << '\n';}
		try {std::cout << B_BLUE "longest() : " RESET << sp.longestSpan() << "\n";}
		catch (std::exception &e) {std::cout << e.what() << '\n';}
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "test short and long :" RESET "\n";
    {
		Span sp = Span(10);
		Span sp1 = Span(10);
		Span sp2 = Span(10);

		sp.addNumber(43);
		sp.addNumber(37);
		sp.addNumber(94);
		sp.addNumber(18);
		sp.addNumber(47);
		sp.addNumber(98);
		sp.addNumber(69);
		sp.addNumber(52);
		sp.addNumber(58);
		sp.addNumber(63);

		sp1.addNumber(43);
		sp1.addNumber(37);
		sp1.addNumber(94);
		sp1.addNumber(18);
		sp1.addNumber(47);
		sp1.addNumber(98);
		sp1.addNumber(47);
		sp1.addNumber(52);
		sp1.addNumber(58);
		sp1.addNumber(63);

		sp2.addNumber(143);
		sp2.addNumber(37);
		sp2.addNumber(94);
		sp2.addNumber(18);
		sp2.addNumber(47);
		sp2.addNumber(98);
		sp2.addNumber(69);
		sp2.addNumber(52);
		sp2.addNumber(58);
		sp2.addNumber(63);

		std::cout << B_BLUE "sp :" RESET "\n";
		std::cout << sp << "\n";
		std::cout << sp.shortestSpan() << "\n";
		std::cout << sp.longestSpan() << "\n";

		std::cout << B_BLUE "sp1 :" RESET "\n";
		std::cout << sp1 << "\n";
		std::cout << sp1.shortestSpan() << "\n";
		std::cout << sp1.longestSpan() << "\n";

		std::cout << B_BLUE "sp2 :" RESET "\n";
		std::cout << sp2 << "\n";
		std::cout << sp2.shortestSpan() << "\n";
		std::cout << sp2.longestSpan() << "\n";
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "test range :" RESET "\n";
	{
		unsigned int	len = 13;
		Span			sp = Span(len);
		int				arr[len];

//		len = sizeof(arr) / sizeof(*arr);
		for (unsigned int i = 0; i < len; i++)
			arr[i] = rand() % 100;

		//sp.addNumber(arr, len);
		sp.addNumber(arr, &arr[len]);

		std::cout << B_BLUE "sp :" RESET "\n";
		std::cout << sp << "\n";
		std::cout << sp.shortestSpan() << "\n";
		std::cout << sp.longestSpan() << "\n";
	}

	return 0;
}

