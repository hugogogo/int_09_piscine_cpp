#include "Span.hpp"

#define COPLIEN_COLOR B_CYAN

/*********************************************
 * CONSTRUCTORS
 *********************************************/

Span::Span( unsigned int N ) : _max(N) {
//	std::cout << COPLIEN_COLOR "Span constructor" RESET "\n";
	return;
}

Span::Span( Span const & src )
: _max(src._max) {
//	std::cout << COPLIEN_COLOR "Span copy constructor" RESET "\n";
	*this = src;
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

Span::~Span() {
//	std::cout << COPLIEN_COLOR "Span destructor" RESET "\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

Span &	Span::operator=( Span const & rhs ) {
	if ( this != &rhs )
	{
		_container = rhs._container;
		if (_container.size() > _max)
			_container.resize(_max);
	}
	return *this;
}

std::ostream &	operator<<(std::ostream & o, Span const & rhs) {
	if ( !rhs.empty() ) {
		o << '[';
		std::copy (rhs.begin(), rhs.end(), std::ostream_iterator<int>(o, ", "));
		o << "\b\b]";
	}
	return o;
}


/*********************************************
 * ACCESSORS
 *********************************************/

//unsigned int	Span::getMax() const {return _max;}
std::vector<int>::const_iterator	Span::begin() const {
	return (_container.begin()); }
std::vector<int>::const_iterator	Span::end() const {
	return (_container.end()); }
bool	Span::empty() const {
	return (_container.empty()); }


/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	Span::addNumber(int nb) {
	if (_container.size() >= _max)
		throw std::out_of_range(B_RED "out of range number" RESET);
	_container.push_back(nb);
	_sort.push_back(nb);
	std::sort(_sort.begin(), _sort.end());
}
//void    Span::addNumber(int * arr, unsigned int len) {
//	for (unsigned int i = 0; i < len; i++) {
//		if (_container.size() >= _max)
//			throw std::out_of_range(B_RED "out of range number" RESET);
//		_container.push_back(arr[i]);
//		_sort.push_back(arr[i]);
//	}
//	std::sort(_sort.begin(), _sort.end());
//}
template <class InputIterator>
void    Span::addNumber(InputIterator first, InputIterator last) {
	if (last < first)
		throw std::out_of_range(B_RED "bad iterators" RESET);
	for (; first != last; first++) {
		if (_container.size() >= _max)
			throw std::out_of_range(B_RED "out of range number" RESET);
		_container.push_back(*first);
		_sort.push_back(*first);
	}
	std::sort(_sort.begin(), _sort.end());
}
template void Span::addNumber<int*>(int*, int*);

unsigned int	Span::shortestSpan() {
	int const			size = _container.size();
	unsigned int		shortest = longestSpan();
	unsigned int		tmp;

	if (_container.size() < 2)
		throw std::length_error(B_RED "need at least 2 elements" RESET);

	for (int i = 0; i < size - 1; i++)
	{
		tmp = _sort[i + 1] - _sort[i];
		if (tmp < shortest)
			shortest = tmp;
	}

	return shortest;
}
unsigned int	Span::longestSpan() {
	if (_container.size() < 2)
		throw std::length_error(B_RED "need at least 2 elements" RESET);
	return (_sort.back() - _sort.front());
}

