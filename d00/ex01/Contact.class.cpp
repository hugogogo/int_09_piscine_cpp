#include "Contact.class.hpp"

Contact::Contact( void ) {
	this->add_first("");
	this->add_last("");
	this->add_nick("");
	this->add_num("");
	this->add_secret("");
	return;
}

Contact::~Contact( void ) {
	return;
}

// ADD
void Contact::add_first( std::string str ) {this->_first.assign(str);}
void Contact::add_last( std::string str ) {this->_last.assign(str);}
void Contact::add_nick( std::string str ) {this->_nick.assign(str);}
void Contact::add_num( std::string str ) {this->_num.assign(str);}
void Contact::add_secret( std::string str ) {this->_secret.assign(str);}

// GET
std::string Contact::get_first( void ) {return _first;}
std::string Contact::get_last( void ) {return _last;}
std::string Contact::get_nick( void ) {return _nick;}
std::string Contact::get_num( void ) {return _num;}
std::string Contact::get_secret( void ) {return _secret;}

// CLEAR
void Contact::clear_contact() {
	this->_first.clear();
	this->_last.clear();
	this->_nick.clear();
	this->_num.clear();
	this->_secret.clear();
}
