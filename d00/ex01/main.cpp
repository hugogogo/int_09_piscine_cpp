#include "PhoneBook.class.hpp"
#include "color.h"

# include <iostream>
# include <string>

int	main() {

	std::string cmd;
	PhoneBook	YellowPage;

	while (std::cin)
	{
		std::cout << std::endl;
		std::cout << B_YELLOW "enter a command: ADD / SEARCH / EXIT" << std::endl;
		std::cout << std::endl << B_GREEN;
		std::getline(std::cin, cmd);
		std::cout << RESET << std::endl;
		if (cmd.compare("ADD") == 0)
			YellowPage.add_contact();
		else if (cmd.compare("SEARCH") == 0)
			YellowPage.search_contact();
		else if (cmd.compare("EXIT") == 0)
			break;
	}

	return 0;
}
