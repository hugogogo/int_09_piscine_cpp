#include "PhoneBook.class.hpp"
#include "Contact.class.hpp"

PhoneBook::PhoneBook( void ) {

	this->_contact_count = 0;
	return;
}

PhoneBook::~PhoneBook( void ) {
	return;
}

void PhoneBook::add_contact() {

	std::string str;
	int	it;

	it = this->_contact_count % 8;
	this->_contact[it].clear_contact();

	std::cout << B_CYAN "enter a first name pliz :" << std::endl;
	while (this->_contact[it].get_first().length() == 0)
	{
		std::cout << B_BLUE "->  " RESET;
		if (!std::getline(std::cin, str))
			return;
		this->_contact[it].add_first(str);
	}

	std::cout << B_CYAN "enter a last name pliz :" << std::endl;
	while (this->_contact[it].get_last().length() == 0)
	{
		std::cout << B_BLUE "->  " RESET;
		if (!std::getline(std::cin, str))
			return;
		this->_contact[it].add_last(str);
	}

	std::cout << B_CYAN "enter a nickname pliz :" << std::endl;
	while (this->_contact[it].get_nick().length() == 0)
	{
		std::cout << B_BLUE "->  " RESET;
		if (!std::getline(std::cin, str))
			return;
		this->_contact[it].add_nick(str);
	}

	std::cout << B_CYAN "enter a number pliz :" << std::endl;
	while (this->_contact[it].get_num().length() == 0)
	{
		std::cout << B_BLUE "->  " RESET;
		if (!std::getline(std::cin, str))
			return;
		this->_contact[it].add_num(str);
	}

	std::cout << B_CYAN "enter a secret of this contact pliz :" << std::endl;
	while (this->_contact[it].get_secret().length() == 0)
	{
		std::cout << B_BLUE "->  " RESET;
		if (!std::getline(std::cin, str))
			return;
		this->_contact[it].add_secret(str);
	}

	this->_contact_count++;
}

std::string truncate(std::string str, size_t len) {

	if (str.length() > len)
		return str.substr(0, len - 1) + ".";
	return str;

}

int PhoneBook::print_phonebook() {

	std::cout << std::endl;
	int	it;

	it = this->_contact_count;
	if (it > 8)
		it = 8;
	it--;

	std::cout << " ------------------------------------------- " << std::endl;
	std::cout << "|" << std::setw(10) << "index";
	std::cout << "|" << std::setw(10) << "first name";
	std::cout << "|" << std::setw(10) << "last name";
	std::cout << "|" << std::setw(10) << "nickname";
	std::cout << "|" << std::endl;
	std::cout << " ---------- ---------- ---------- ---------- " << std::endl;
	for (int i = 0; i <= it; i++)
	{
		std::cout << "|" << std::setw(10) << i;
		std::cout << "|" << std::setw(10) << truncate(_contact[i].get_first(), 10);
		std::cout << "|" << std::setw(10) << truncate(_contact[i].get_last(), 10);
		std::cout << "|" << std::setw(10) << truncate(_contact[i].get_nick(), 10);
		std::cout << "|" << std::endl;
	}
	if (this->_contact_count == 0)
		std::cout << "|                   EMPTY                   |" << std::endl;
	std::cout << " ------------------------------------------- " << std::endl;
	std::cout << std::endl;
	return this->_contact_count;
}

void PhoneBook::print_contact( int id ) {

	std::cout << std::endl;
	std::cout << B_PURPLE "FIRST NAME : " << RESET << this->_contact[id].get_first()  << std::endl;
	std::cout << B_PURPLE " LAST NAME : " << RESET << this->_contact[id].get_last()   << std::endl;
	std::cout << B_PURPLE "  NICKNAME : " << RESET << this->_contact[id].get_nick()   << std::endl;
	std::cout << B_PURPLE "    NUMBER : " << RESET << this->_contact[id].get_num()    << std::endl;
	std::cout << B_PURPLE "    SECRET : " << RESET << this->_contact[id].get_secret() << std::endl;

}

void PhoneBook::search_contact() {

	std::string index;
	long	id;
	char	*ret;

	if (this->print_phonebook() == 0)
		return;
	std::cout << std::endl << B_CYAN "choose an index :" << std::endl;
	while (1)
	{
		std::cout << B_BLUE "->  " RESET;
		if (!std::getline(std::cin, index))
			return;
		id = std::strtol(index.c_str(), &ret, 10);
		if (*ret || index.empty() || id < 0 || id > _contact_count - 1 || id > 7)
			std::cout << B_RED "sorry, not a valid index" RESET << std::endl;
		else
			break;
	}
	this->print_contact(id);

}
