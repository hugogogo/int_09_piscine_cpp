#ifndef CONTACT_CLASS_HPP
# define CONTACT_CLASS_HPP

# include <iostream>
# include <string>

class Contact {

public:

	Contact();
	~Contact();

	void add_first(std::string str);
	void add_last (std::string str);
	void add_nick (std::string str);
	void add_num(std::string str);
	void add_secret(std::string str);

	std::string get_first();
	std::string get_last ();
	std::string get_nick ();
	std::string get_num();
	std::string get_secret();

	void clear_contact();

	void print_contact();

private:

	std::string _first;
	std::string _last;
	std::string _nick;
	std::string _num;
	std::string _secret;

};

#endif

/*
 *   create_contact (first ; last ; nick ; num ; secret)
 *   get_first
 *   get_last
 *   get_nick
 *   print_contact
 */
