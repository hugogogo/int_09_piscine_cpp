#ifndef PHONEBOOK_CLASS_HPP
# define PHONEBOOK_CLASS_HPP

#include "Contact.class.hpp"
#include "color.h"

#include <cstring>
#include <iomanip>
#include <cstdlib>

class PhoneBook {

	public:

		PhoneBook();
		~PhoneBook();

		void	add_contact();
		void	search_contact();
		int		print_phonebook();
		void	print_contact( int id);

	private:

		Contact	_contact[8];
		int		_contact_count;

};

#endif
