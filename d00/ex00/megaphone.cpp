#include <iostream>

int main(int ac, char **av)
{
    if (ac < 2)
        std::cout << std::uppercase << "* LOUD AND UNBEARABLE FEEDBACK NOISE *";
    else
        for (int i = 1; av[i] != NULL; i++)
            for (int j = 0; av[i][j] != '\0'; j++)
                std::cout << (char)toupper(av[i][j]);
    std::cout << std::endl;
    return 0;
}
