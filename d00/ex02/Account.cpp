#include "Account.hpp"
#include <iostream>
#include <iomanip>
#include <ctime>

// to compare output : diff <(cut -d \  -f 2 file1) <(cut -d \  -f 2 file2)
// it's process substitution : https://www.gnu.org/software/bash/manual/html_node/Process-Substitution.html

void	Account::displayAccountsInfos( void ) {

	_displayTimestamp();
	std::cout << "accounts:" << _nbAccounts << ";";
	std::cout << "total:" << _totalAmount << ";";
	std::cout << "deposits:" << _totalNbDeposits << ";";
	std::cout << "withdrawals:" << _totalNbWithdrawals << std::endl;
}

Account::Account( int initial_deposit ) {

	this->_accountIndex = this->_nbAccounts;
	this->_nbAccounts++;
	this->_amount = initial_deposit;
	this->_totalAmount += initial_deposit;
	this->_nbDeposits = 0;
	this->_nbWithdrawals = 0;

	this->_displayTimestamp();
	std::cout << "index:"  << this->_accountIndex << ";";
	std::cout << "amount:" << this->_amount << ";";
	std::cout << "created" << std::endl;

	return;

}

Account::~Account( void ) {

	this->_displayTimestamp();
	std::cout << "index:"  << this->_accountIndex << ";";
	std::cout << "amount:" << this->_amount << ";";
	std::cout << "closed"  << std::endl;

	return;
}

void	Account::makeDeposit( int deposit ) {

	this->_displayTimestamp();
	std::cout << "index:"       << this->_accountIndex << ";";
	std::cout << "p_amount:"    << this->_amount << ";";
	std::cout << "deposit:"     << deposit << ";";

	this->_totalNbDeposits++;
	this->_nbDeposits++;
	this->_amount += deposit;
	this->_totalAmount += deposit;

	std::cout << "amount:"      << this->_amount << ";";
	std::cout << "nb_deposits:" << this->_nbDeposits << std::endl;
}

bool	Account::makeWithdrawal( int withdrawal ) {

	this->_displayTimestamp();
	std::cout << "index:"          << this->_accountIndex << ";";
	std::cout << "p_amount:"       << this->_amount << ";";

	if (withdrawal > this->_amount)
	{
		std::cout << "withdrawal:refused" << std::endl;
		return false;
	}
	this->_totalNbWithdrawals++;
	this->_nbWithdrawals++;
	this->_amount -= withdrawal;
	this->_totalAmount -= withdrawal;

	std::cout << "withdrawal:"     << withdrawal << ";";
	std::cout << "amount:"         << this->_amount << ";";
	std::cout << "nb_withdrawals:" << this->_nbWithdrawals << std::endl;

	return true;
}

void	Account::displayStatus( void ) const {

	this->_displayTimestamp();
	std::cout << "index:"       << this->_accountIndex << ";";
	std::cout << "amount:"      << this->_amount << ";";
	std::cout << "deposits:"    << this->_nbDeposits << ";";
	std::cout << "withdrawals:" << this->_nbWithdrawals << std::endl;

}

void	Account::_displayTimestamp( void ) {

	time_t rawtime = time(NULL);
	struct tm *stamp = localtime(&rawtime);
	std::cout << "[";
	std::cout << std::setfill('0') << std::setw(2) << stamp->tm_year + 1900;
	std::cout << std::setfill('0') << std::setw(2) << stamp->tm_mon;
	std::cout << std::setfill('0') << std::setw(2) << stamp->tm_mday << "_";
	std::cout << std::setfill('0') << std::setw(2) << stamp->tm_hour;
	std::cout << std::setfill('0') << std::setw(2) << stamp->tm_min;
	std::cout << std::setfill('0') << std::setw(2) << stamp->tm_sec;
	std::cout << "] ";

}

int	Account::_totalAmount = 0;
int	Account::_nbAccounts = 0;
int	Account::_totalNbDeposits = 0;
int	Account::_totalNbWithdrawals = 0;

int	Account::checkAmount( void ) const {return 0;}
int	Account::getNbAccounts( void ) {return 0;}
int	Account::getTotalAmount( void ) {return 0;}
int	Account::getNbDeposits( void ) {return 0;}
int	Account::getNbWithdrawals( void ) {return 0;}

Account::Account( void ) {return;}

