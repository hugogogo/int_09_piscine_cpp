#include <iostream>
#include <string>

#include "convert.h"

// 2^24 = 16777216;
// 2^31 = 2147483648
// first char printable "!" -> 21 (space -> 20)

int	main(int ac, char **av) {

	if (ac > 1)
	{
		convert(av[1]);
		return 0;
	}

	std::cout << "\n\n" B_GREEN "----------------------------------------------------\n"
			  << "\nCHAR" RESET "\n";
	// char
	convert("!");
	convert("\"");
	convert("#");
	convert("$");
	convert("%");
	convert("&");
	convert("'");
	convert("(");
	convert(")");
	convert("*");
	convert("+");
	convert(",");
	convert("-");
	convert(".");
	convert("/");
	convert(":");
	convert(";");
	convert("<");
	convert("=");
	convert(">");
	convert("?");
	convert("@");
	convert("A");
	convert("B");
	convert("C");
	convert("D");
	convert("E");
	convert("F");
	convert("G");
	convert("H");
	convert("I");
	convert("J");
	convert("K");
	convert("L");
	convert("M");
	convert("N");
	convert("O");
	convert("P");
	convert("Q");
	convert("R");
	convert("S");
	convert("T");
	convert("U");
	convert("V");
	convert("W");
	convert("X");
	convert("Y");
	convert("Z");
	convert("[");
	convert("\\");
	convert("]");
	convert("^");
	convert("_");
	convert("`");
	convert("a");
	convert("b");
	convert("c");
	convert("d");
	convert("e");
	convert("f");
	convert("g");
	convert("h");
	convert("i");
	convert("j");
	convert("k");
	convert("l");
	convert("m");
	convert("n");
	convert("o");
	convert("p");
	convert("q");
	convert("r");
	convert("s");
	convert("t");
	convert("u");
	convert("v");
	convert("w");
	convert("x");
	convert("y");
	convert("z");
	convert("{");
	convert("|");
	convert("}");
	convert("~");

	std::cout << "\n\n" B_GREEN "----------------------------------------------------\n"
			  << "\nINT" RESET "\n";
	// int
	convert("0");
	convert("-42");
	convert("42");
	convert("-1");
	convert("1");
	convert("2");
	convert("3");
	convert("4");
	convert("5");
	convert("29");
	convert("30");
	convert("31");
	convert("32");
	convert("33");
	convert("34");
	convert("35");
	convert("122");
	convert("123");
	convert("124");
	convert("125");
	convert("126");
	convert("127");
	convert("128");
	convert("129");
	convert("130");
	convert("253");
	convert("254");
	convert("255");
	convert("256");
	convert("257");
	convert("258");

	std::cout << "\n\n" B_PURPLE "[min - max int]" RESET "\n";
	convert(MIN_I_5);
	convert(MIN_I_4);
	convert(MIN_I_3);
	convert(MIN_I_2);
	convert(MIN_I_1);
	convert(MIN_INT);
	convert(MIN_I__1);
	convert(MIN_I__2);
	convert(MIN_I__3);
	convert(MIN_I__4);
	convert(MIN_I__5);

	convert(MAX_I__5);
	convert(MAX_I__4);
	convert(MAX_I__3);
	convert(MAX_I__2);
	convert(MAX_I__1);
	convert(MAX_INT);
	convert(MAX_INT_1);
	convert(MAX_I_2);
	convert(MAX_I_3);
	convert(MAX_I_4);
	convert(MAX_I_5);
	std::cout << "\n" B_PURPLE "[END min - max int]" RESET "\n\n";

	convert(MAX_FLOAT_INT_PRECISION);
	convert(MAX_FLOAT_INT_PREC__1);
	convert(MAX_FLOAT_INT_PREC__2);
	convert(MAX_FLOAT_INT_PREC__3);
	convert(MAX_FLOAT_INT_PREC__4);
	convert(MAX_FLOAT_INT_PREC_1);
	convert(MAX_FLOAT_INT_PREC_2);
	convert(MAX_FLOAT_INT_PREC_3);
	convert(MAX_FLOAT_INT_PREC_4);
	convert(MAX_FLOAT_INT_PREC_5);
	convert(MAX_FLOAT_INT_PREC_6);

	std::cout << "\n\n" B_GREEN "----------------------------------------------------\n"
			  << "\nFLOAT" RESET "\n";
	// float
	convert("0.0f");
	convert("-4.2f");
	convert("4.2f");
	convert("-inff");
	convert("+inff");
	convert("nanf");

	std::cout << "\n\n" B_PURPLE "[min - max int]" RESET "\n";
	convert(MIN_I_5  ".0f");
	convert(MIN_I_4  ".0f");
	convert(MIN_I_3  ".0f");
	convert(MIN_I_2  ".0f");
	convert(MIN_I_1  ".0f");
	convert(MIN_INT  ".0f");
	convert(MIN_I__1 ".0f");
	convert(MIN_I__2 ".0f");
	convert(MIN_I__3 ".0f");
	convert(MIN_I__4 ".0f");
	convert(MIN_I__5 ".0f");

	convert(MAX_I__5  ".0f");
	convert(MAX_I__4  ".0f");
	convert(MAX_I__3  ".0f");
	convert(MAX_I__2  ".0f");
	convert(MAX_I__1  ".0f");
	convert(MAX_INT   ".0f");
	convert(MAX_INT_1 ".0f");
	convert(MAX_I_2   ".0f");
	convert(MAX_I_3   ".0f");
	convert(MAX_I_4   ".0f");
	convert(MAX_I_5   ".0f");
	std::cout << "\n" B_PURPLE "[END min - max int]" RESET "\n\n";

	convert(MAX_FLOAT_INT_PRECISION".0f");
	convert(MAX_FLOAT_INT_PREC__1".0f");
	convert(MAX_FLOAT_INT_PREC__2".0f");
	convert(MAX_FLOAT_INT_PREC__3".0f");
	convert(MAX_FLOAT_INT_PREC__4".0f");
	convert(MAX_FLOAT_INT_PREC_1".0f");
	convert(MAX_FLOAT_INT_PREC_2".0f");
	convert(MAX_FLOAT_INT_PREC_3".0f");
	convert(MAX_FLOAT_INT_PREC_4".0f");
	convert(MAX_FLOAT_INT_PREC_5".0f");
	convert(MAX_FLOAT_INT_PREC_6".0f");
	convert(MAX_FLOAT".0f");
	convert(MAX_F__1".0f");
	convert(MAX_F__2".0f");
	convert(MAX_F__3".0f");
	convert(MAX_F__4".0f");
	convert(MAX_F_1".0f");
	convert(MAX_F_2".0f");
	convert(MAX_F_3".0f");
	convert(MAX_F_4".0f");
	convert(MAX_F_5".0f");
	convert(MAX_F_6".0f");
	convert(MAX_F_N".0f");

	std::cout << "\n\n" B_GREEN "----------------------------------------------------\n"
			  << "\nDOUBLE" RESET "\n";
	//double
	convert("0.0");
	convert("-4.2");
	convert("4.2");
	convert("-inf");
	convert("+inf");
	convert("nan");

	std::cout << "\n\n" B_PURPLE "[min - max int]" RESET "\n";
	convert(MIN_I_5  ".0");
	convert(MIN_I_4  ".0");
	convert(MIN_I_3  ".0");
	convert(MIN_I_2  ".0");
	convert(MIN_I_1  ".0");
	convert(MIN_INT  ".0");
	convert(MIN_I__1 ".0");
	convert(MIN_I__2 ".0");
	convert(MIN_I__3 ".0");
	convert(MIN_I__4 ".0");
	convert(MIN_I__5 ".0");

	convert(MAX_I__5  ".0");
	convert(MAX_I__4  ".0");
	convert(MAX_I__3  ".0");
	convert(MAX_I__2  ".0");
	convert(MAX_I__1  ".0");
	convert(MAX_INT   ".0");
	convert(MAX_INT_1 ".0");
	convert(MAX_I_2   ".0");
	convert(MAX_I_3   ".0");
	convert(MAX_I_4   ".0");
	convert(MAX_I_5   ".0");
	std::cout << "\n" B_PURPLE "[END min - max int]" RESET "\n\n";

	convert(MAX_FLOAT_INT_PRECISION".0");
	convert(MAX_FLOAT_INT_PREC__1".0");
	convert(MAX_FLOAT_INT_PREC__2".0");
	convert(MAX_FLOAT_INT_PREC__3".0");
	convert(MAX_FLOAT_INT_PREC__4".0");
	convert(MAX_FLOAT_INT_PREC_1".0");
	convert(MAX_FLOAT_INT_PREC_2".0");
	convert(MAX_FLOAT_INT_PREC_3".0");
	convert(MAX_FLOAT_INT_PREC_4".0");
	convert(MAX_FLOAT_INT_PREC_5".0");
	convert(MAX_FLOAT_INT_PREC_6".0");
	convert(MAX_FLOAT".0");
	convert(MAX_F__1".0");
	convert(MAX_F__2".0");
	convert(MAX_F__3".0");
	convert(MAX_F__4".0");
	convert(MAX_F_1".0");
	convert(MAX_F_2".0");
	convert(MAX_F_3".0");
	convert(MAX_F_4".0");
	convert(MAX_F_5".0");
	convert(MAX_F_6".0");
	convert(MAX_F_N".0");
	convert(MAX_DOUBLE".0");
	convert(MIN_DOUBLE".0");
	convert(MAX_D__1".0");
	convert(MAX_D__2".0");
	convert(MAX_D__3".0");
	convert(MAX_D__4".0");
	convert(MAX_D_1".0");
	convert(MAX_D_2".0");
	convert(MAX_D_3".0");
	convert(MAX_D_4".0");
	convert(MAX_D_5".0");
	convert(MAX_D_6".0");

	return 0;
}

