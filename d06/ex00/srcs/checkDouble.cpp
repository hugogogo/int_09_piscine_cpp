#include "convert.h"

void	fromDouble(double value) {
	// char
	toChar(value);
	// int
	toInt(value);
	// float
	toFloat(value);
}

bool	isDouble(std::string str) {

	size_t	l;

	if (str[0] == '+' || str[0] == '-')
		str.erase(str.begin());
	if (str.length() == 0)
		return false;
	if (!str.compare("inf") || !str.compare("nan"))
		return true;
	l = str.find_first_not_of("0123456789");
	if (l == std::string::npos || str[l] != '.' || l > DOUBLE_MAX_LENGTH)
		return false;
	if (str.find_first_not_of("0123456789", l + 1) != std::string::npos)
		return false;
	if (l < DOUBLE_MAX_LENGTH)
		return true;
	if (str.compare(0, l, MAX_DOUBLE) > 0)
		return false;


	return true;
}

bool	checkDouble(std::string str) {
	double	d;

	if (!isDouble(str))
		return false;

//	std::istringstream(str) >> d;
	d = strtod(str.c_str(), NULL);
	std::cout << std::fixed << B_CYAN << d;
//	printDot(d);
	std::cout << B_YELLOW " double" RESET "\n";
	fromDouble(d);

	return true;
}
