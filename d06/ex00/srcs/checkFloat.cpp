#include "convert.h"

static void	fromFloat(float value) {
	// char
	toChar(value);
	// int
	toInt(value);
	// double
	toDouble(value);
}

static bool	isFloat(std::string str) {
	size_t		l;
	size_t		l2;

	if (str[0] == '+' || str[0] == '-')
		str.erase(str.begin());
	if (str.length() == 0)
		return false;
	if (!str.compare("inff") || !str.compare("nanf"))
		return true;
	l = str.find_first_not_of("0123456789");
	if (l == std::string::npos || str[l] != '.' || l > FLOAT_MAX_LENGTH)
		return false;
	l2 = str.find_first_not_of("0123456789", l + 1);
	if (l2 == std::string::npos || l2 != str.length() - 1 || str[l2] != 'f')
		return false;
	if (l < FLOAT_MAX_LENGTH)
		return true;
	if (str.compare(0, l, MAX_FLOAT) > 0)
		return false;

	return true;
}

bool	checkFloat(std::string str) {
	float	f;

	if (!isFloat(str))
		return false;

//	std::istringstream(str) >> f;
	f = strtod(str.c_str(), NULL);
	std::cout << std::fixed << B_CYAN << f;
//	printDot(f);
	std::cout << "f" << B_YELLOW " float" RESET "\n";
	fromFloat(f);

	return true;
}
