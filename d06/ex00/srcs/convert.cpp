#include <iostream>
#include <string>
#include "convert.h"

//   f[]       : is an array
//  *f[]       : is an array of pointers
// (*f[])()    : is an array of pointers to functions with no parameters
// (*f[])(str) : is an array of pointers to functions with parameter str
// see :
// https://stackoverflow.com/questions/31643245/declaring-an-array-of-functions-of-type-void-c
bool	(*checkFunc[])(std::string str) =
{
	checkChar,
	checkInt,
	checkFloat,
	checkDouble,
};

void	convert(std::string str) {

	std::cout << "\n" B_BLUE << str << RESET "\n";
	int size = sizeof(checkFunc) / sizeof(checkFunc[0]);
	
	for (int it = 0; it < size; it++)
		if ((*checkFunc[it])(str))
			return ;
	std::cout << B_RED "is not valid type for this exercise" RESET "\n";
}
