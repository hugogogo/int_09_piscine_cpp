#include "convert.h"

void	fromInt(int value) {
	// char
	toChar(value);
	// float
	toFloat(value);
	// double
	toDouble(value);
}

bool	isInt(std::string str) {
	std::string	int_xtrem = MAX_INT;

	if (str[0] == '-')
		int_xtrem = MAX_INT_1;
	if (str[0] == '+' || str[0] == '-')
		str.erase(str.begin());
	if (str.length() == 0 || str.length() > INT_MAX_LENGTH)
		return false;
	if (str.find_first_not_of("0123456789") != std::string::npos)
		return false;
	if (str.length() < INT_MAX_LENGTH)
		return true;
	if (str.compare(int_xtrem) > 0)
		return false;

	return true;
}

bool	checkInt(std::string str) {
	int	i;

	if (!isInt(str))
		return false;

	std::istringstream(str) >> i;
	std::cout << B_CYAN << i << B_YELLOW " int" RESET "\n";
	fromInt(i);

	return true;
}
