#include "convert.h"
#include <cfloat>

// tried to check for NaN and INF
//static bool	is_inf_pos( float a )	{ return ( *(((long*)(&a))+1) & 0xFFFFFFFFU ) == 0x7F800000U; }
//static bool	is_inf_neg( float a )	{ return ( *(((long*)(&a))+1) & 0xFFFFFFFFU ) == 0xFF800000U; }
//static bool	is_nan( float a )		{ return (( *(((long*)(&a))+1) & 0x7FF80000L ) == 0x7FF800000L); }

// https://isocpp.org/wiki/faq/templates#templates-defn-vs-decl
template <typename T>
void	toChar(T value) {
	std::cout << std::setw(7) << std::left << "char";
//	if (value < 0 || value > std::numeric_limits<char>::max())
	if (!(value >= 0 && value <= std::numeric_limits<char>::max()))
		std::cout << B_RED << "impossible" << RESET "\n";
	else if (!isprint(value))
		std::cout << B_RED << "non displayable" << RESET "\n";
	else
		std::cout << B_CYAN << static_cast<char>(value) << RESET "\n";
}

template <typename T>
void	toInt(T value) {
	std::cout << std::setw(7) << std::left << "int";
	if (! (value <= std::numeric_limits<int>::max()
		&& value >= std::numeric_limits<int>::min()) )
	{
		std::cout << B_RED << "impossible" << RESET "\n";
		return ;
	}
		std::cout << B_CYAN << static_cast<int>(value) << RESET "\n";
}

template <typename T>
void	toFloat(T value) {
	std::cout << std::setw(7) << std::left << "float";
	// tricks to check if is nan and infinity
	// see : https://www.jacksondunstan.com/articles/983
	// and : https://stackoverflow.com/questions/2249110/how-do-i-make-a-portable-isnan-isinf-function
	if (!((value * 0) != 0))
		// min() ne renvoit que 0, il faut utiliser lowest() (c++11) ou -max()
		// https://stackoverflow.com/questions/17070351/why-does-numeric-limitsmin-return-a-negative-value-for-int-but-positive-values
		// https://en.cppreference.com/w/cpp/types/numeric_limits/min
		if (! (value <= std::numeric_limits<float>::max()
			&& value >= -(std::numeric_limits<float>::max())) )
		{
			std::cout << B_RED << "impossible" << RESET "\n";
			return ;
		}
	std::cout << B_CYAN << std::fixed << static_cast<float>(value);
//	printDot(value);
	std::cout << "f" << RESET "\n";
}

template <typename T>
void	toDouble(T value) {
	std::cout << std::setw(7) << std::left << "double" << B_CYAN
			  << std::fixed << static_cast<double>(value);
//	printDot(value);
	std::cout << RESET "\n";
}

//template <typename T>
//void	printDot(T value) {
//	std::ostringstream	ostr;
//	ostr << value;
//	if (ostr.str().compare("inf")
//		&& ostr.str().compare("-inf")
//		&& ostr.str().compare("nan"))
//		if ((ostr.str().find('.')) == std::string::npos)
//			std::cout << ".0";
//}

template void toChar<int>(int);
template void toChar<float>(float);
template void toChar<double>(double);

template void toInt<float>(float);
template void toInt<double>(double);

template void toFloat<int>(int);
template void toFloat<double>(double);

template void toDouble<int>(int);
template void toDouble<float>(float);

//template void printDot<float>(float);
//template void printDot<double>(double);
