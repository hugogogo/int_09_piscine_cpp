#include "convert.h"

void	fromChar(char c) {
	int		i;
	float	f;
	double	d;

	i = static_cast<int>(c);
	std::cout << std::setw(7) << std::left << "int" << B_CYAN << i << RESET "\n";
	f = static_cast<float>(c);
	std::cout << std::setw(7) << std::left << "float" << B_CYAN << f << RESET "\n";
	d = static_cast<double>(c);
	std::cout << std::setw(7) << std::left << "double" << B_CYAN << d << RESET "\n";
}

bool	isChar(std::string str) {
	if (str.length() != 1 || isdigit(str[0]))
		return false;
	return true;
}

bool	checkChar(std::string str) {
	char	c;

	if (!isChar(str))
		return false;

	c = str[0];
	std::cout << B_CYAN << c << B_YELLOW " char" RESET "\n";
	fromChar(c);

	return true;
}
