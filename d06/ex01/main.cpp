#include <iostream>
#include <string>
#include <stdint.h>
#include "colors.h"

#include "Data.hpp"

#define N_TEST "3"

template < typename T >
void	printBits(T num)
{
	unsigned long int	*p = reinterpret_cast<unsigned long int *>(&num);

	for (unsigned long int mask = 1LU << (sizeof(T) *8 -1); mask; mask >>= 1)
		std::cout << ((*p & mask) != 0);
}

uintptr_t serialize(Data* ptr) {
	return ( reinterpret_cast<uintptr_t>(ptr) );
}
Data* deserialize(uintptr_t raw) {
	return ( reinterpret_cast<Data*>(raw) );
}

int	main() {

	int	i = 0;

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "test basique :" RESET "\n";
	{
		Data data;
		Data * data_ptr = & data;
		uintptr_t raw;
		data.str = "42";
	
		raw = serialize(data_ptr);
		std::cout << " " << raw << "\n";

//		unsigned long int *li = (unsigned long int *)&data_ptr;
//		std::cout << " " << *li << "\n";

		data_ptr = deserialize(raw);
		std::cout << " " << data_ptr << "\n";
	
		std::cout << data_ptr->str << "\n";
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "test with bits prints :" RESET "\n";
	{
		Data data;
		Data * data_ptr = & data;
		uintptr_t raw;
		data.str = "hello you I see you";
	
		raw = serialize(data_ptr);
		printBits(raw);
		std::cout << " " << raw << "\n";
	
		data_ptr = deserialize(raw);
		printBits(data_ptr);
		std::cout << " " << data_ptr << "\n";
	
		std::cout << data_ptr->str << "\n";
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "test with special string :" RESET "\n";
	{
		Data data;
		Data * data_ptr = & data;
		uintptr_t raw;
		data.str = "                  lIUJLIJLIJLI lijlij                             ";
	
		raw = serialize(data_ptr);
		printBits(raw);
		std::cout << " " << raw << "\n";
	
		data_ptr = deserialize(raw);
		printBits(data_ptr);
		std::cout << " " << data_ptr << "\n";
	
		std::cout << data_ptr->str << "\n";
	}

	return 0;
}

