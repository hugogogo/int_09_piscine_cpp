#include "Form.hpp"

#define COPLIEN_COLOR B_CYAN

/*********************************************
 * CONSTRUCTORS
 *********************************************/

Form::Form( std::string name, int signedGrade, int executeGrade )
: _name(name)
, _signed(false)
, _signedGrade(signedGrade)
, _executeGrade(executeGrade) {
	if (signedGrade > 150 || executeGrade > 150)
		throw Form::GradeTooLowException();
	if (signedGrade < 1 || executeGrade < 1)
		throw Form::GradeTooHighException();
	std::cout << COPLIEN_COLOR "Form constructor" RESET "\n";
	return;
}

Form::Form( Form const & src )
: _name(src.getName())
, _signedGrade(src.getSignedGrade())
, _executeGrade(src.getExecuteGrade()) {
	std::cout << COPLIEN_COLOR "Form copy constructor" RESET "\n";
	*this = src;
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

Form::~Form() {
	std::cout << COPLIEN_COLOR "Form destructor" RESET "\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

Form &	Form::operator=( Form const & rhs ) {
	if ( this != &rhs ) {
		_signed = rhs.getSigned();
	}
	return *this;
}

std::ostream &	operator<<(std::ostream & o, Form const & rhs)
{
	o	<< "form: "
		<< rhs.getName() << ", is signed: "
		<< rhs.getSigned() << ", need at least ["
		<< rhs.getSignedGrade() << "] to be signed, need at least ["
		<< rhs.getExecuteGrade() << "] to be executed";
	return (o);
}

/*********************************************
 * ACCESSORS
 *********************************************/

std::string	Form::getName() const {return _name;}
bool		Form::getSigned() const {return _signed;}
int 		Form::getSignedGrade() const {return _signedGrade;}
int 		Form::getExecuteGrade() const {return _executeGrade;}

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	Form::beSigned( Bureaucrat const & b) {
	if (b.getGrade() < _signedGrade)
		_signed = true;
	else
		throw Form::GradeTooLowException();
}

/*********************************************
 * NESTED CLASS
 *********************************************/

const char * Form::GradeTooHighException::what() const throw() {
	return (B_RED "grade too high" RESET);
}
const char * Form::GradeTooLowException::what() const throw() {
	return (B_RED "grade too low" RESET);
}
