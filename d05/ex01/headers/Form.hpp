#ifndef FORM_HPP
# define FORM_HPP

# include "color.h"
# include <iostream>
# include <string>

class Bureaucrat;
# include "Bureaucrat.hpp"

class Form {

public:

	Form( std::string name, int signedGrade, int executeGrade );
	Form( Form const & src );
	~Form();
	Form &	operator=( Form const & rhs );

	std::string	getName() const;
	bool		getSigned() const;
	int			getSignedGrade() const;
	int			getExecuteGrade() const;

	void	beSigned( Bureaucrat const & b );

private:

	Form();

	class GradeTooHighException : public std::exception {
		const char * what() const throw();};
	class GradeTooLowException : public std::exception {
		const char * what() const throw();};

	std::string const	_name;
	bool				_signed;
	int const			_signedGrade;
	int const			_executeGrade;

};

std::ostream &	operator<<(std::ostream & o, Form const & rhs);

#endif

