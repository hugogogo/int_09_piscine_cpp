#include "Bureaucrat.hpp"
#define N_TEST "5"

int	main() {

	std::cout << B_YELLOW "\n[1/" N_TEST "] test too high :" RESET "\n";
	{
		try {
			Bureaucrat	b("clarence", 151);
		}
		catch (std::exception& e) {
			std::cout << e.what() << "\n";
		}
	}

	std::cout << B_YELLOW "\n[2/" N_TEST "] test signe ko :" RESET "\n";
	{
		{
			Bureaucrat	b("mikel", 50);
			Form		f("serpa_hell", 30, 100);

			std::cout << f << '\n';
			std::cout << b << '\n';
				std::cout << B_BLUE "b.signForm :" RESET "\n";
			b.signForm(f);
			std::cout << f << '\n';
		}
	}

	std::cout << B_YELLOW "\n[3/" N_TEST "] test signe ok :" RESET "\n";
	{
		{
			Bureaucrat	b("coran", 50);
			Form		f("serpa_death", 70, 100);

			std::cout << f << '\n';
			std::cout << b << '\n';
				std::cout << B_BLUE "b.signForm :" RESET "\n";
			b.signForm(f);
			std::cout << f << '\n';
		}
	}

	std::cout << B_YELLOW "\n[4/" N_TEST "] test form too high :" RESET "\n";
	{
		try {
			Form		f("serpa_horror", 155, 100);
		}
		catch (std::exception& e) {
			std::cout << e.what() << "\n";
		}
	}

	std::cout << B_YELLOW "\n[5/" N_TEST "] test form too low :" RESET "\n";
	{
		try {
			Form		f("serpa_starvation", 70, 0);
		}
		catch (std::exception& e) {
			std::cout << e.what() << "\n";
		}
	}

	std::cout << "\n";
	return 0;
}
