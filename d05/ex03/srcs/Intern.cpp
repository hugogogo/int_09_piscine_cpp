#include "Intern.hpp"

#define COPLIEN_COLOR B_CYAN

/*********************************************
 * STATICS
 *********************************************/

const t_formModel	Intern::_chooseForm[] = {
	{"shrubbery", Intern::makeShrubbery},
	{"presidential", Intern::makePresidential},
	{"robotomy", Intern::makeRobotomy}
};

/*********************************************
 * CONSTRUCTORS
 *********************************************/

Intern::Intern() {
	std::cout << COPLIEN_COLOR "Intern constructor" RESET "\n";
	return;
}

Intern::Intern( Intern const & src __attribute__((unused))) {
	std::cout << COPLIEN_COLOR "Intern copy constructor" RESET "\n";
	*this = src;
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

Intern::~Intern() {
	std::cout << COPLIEN_COLOR "Intern destructor" RESET "\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

Intern &	Intern::operator=( Intern const & rhs __attribute__((unused))) {
	return *this;
}

/*********************************************
 * PRIVATE MEMBER FUNCTIONS
 *********************************************/

AForm *	Intern::makeForm(std::string formName, std::string formTarget) const {
	unsigned int len = sizeof _chooseForm / sizeof _chooseForm[0];

	for (unsigned int i = 0; i < len; i++)
		if (isValidForm(formName, i))
		{
			std::cout << "Intern creates " << formName << "\n";
			return _chooseForm[i].create(formTarget);
		}
	std::cout << "Intern cannot create " << formName << "\n";
	return NULL;
}

bool	Intern::isValidForm(std::string name, unsigned int i) const {
	// to lowercase :
	// https://stackoverflow.com/questions/313970/how-to-convert-an-instance-of-stdstring-to-lower-case
	std::transform(name.begin(), name.end(), name.begin(), ::tolower);

	if (name.find(_chooseForm[i].name) == std::string::npos)
		return false;
	return true;
}

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

AForm *	Intern::makeShrubbery(std::string const &target) {
	return new ShrubberyCreationForm(target);}
AForm *	Intern::makePresidential(std::string const &target) {
	return new PresidentialPardonForm(target);}
AForm *	Intern::makeRobotomy(std::string const &target) {
	return new RobotomyRequestForm(target);}

