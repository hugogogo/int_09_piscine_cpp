#include "ShrubberyCreationForm.hpp"

#define COPLIEN_COLOR B_CYAN

/*********************************************
 * CONSTRUCTORS
 *********************************************/

ShrubberyCreationForm::ShrubberyCreationForm( std::string target )
: AForm("shrubbery_creation", target, 145, 137){
	std::cout << COPLIEN_COLOR "ShrubberyCreationForm constructor" RESET "\n";
	return;
}

ShrubberyCreationForm::ShrubberyCreationForm( ShrubberyCreationForm const & src )
: AForm("shrubbery_creation", src.getTarget(), 145, 137) {
	std::cout << COPLIEN_COLOR "ShrubberyCreationForm copy constructor" RESET "\n";
	*this = src;
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

ShrubberyCreationForm::~ShrubberyCreationForm() {
	std::cout << COPLIEN_COLOR "ShrubberyCreationForm destructor" RESET "\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

ShrubberyCreationForm &	ShrubberyCreationForm::operator=( ShrubberyCreationForm const & rhs ) {
	AForm::operator=(rhs);
	return *this;
}

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	ShrubberyCreationForm::formAction() const {
	std::string		name = _target + "_shrubbery";
	std::ofstream	ofs(name.c_str(), std::ofstream::out);

	if (!ofs)
	{
		std::cout << "opening Shrubbery.txt file failed\n";
		return ;
	}

	ofs << "	              * *\n";
	ofs << "	           *    *  *\n";
	ofs << "	      *  *    *     *  *\n";
	ofs << "	     *     *    *  *    *\n";
	ofs << "	 * *   *    *    *    *   *\n";
	ofs << "	 *     *  *    * * .#  *   *\n";
	ofs << "	 *   *     * #.  .# *   *\n";
	ofs << "	  *     \"#.  #: #\" * *    *\n";
	ofs << "	 *   * * \"#. ##\"       *\n";
	ofs << "	   *       \"###\n";
	ofs << "	             \"##\n";
	ofs << "	              ##.\n";
	ofs << "	              .##:\n";
	ofs << "	              :###\n";
	ofs << "	              ;###\n";
	ofs << "	            ,####.\n";
	ofs << "	/\\/\\/\\/\\/\\/.######.\\/\\/\\/\\/\\\n";

	ofs.close();
}

