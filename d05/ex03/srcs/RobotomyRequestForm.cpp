#include "RobotomyRequestForm.hpp"

#define COPLIEN_COLOR B_CYAN

/*********************************************
 * CONSTRUCTORS
 *********************************************/

RobotomyRequestForm::RobotomyRequestForm( std::string target )
: AForm("robotomy_creation", target, 72, 45){
	std::cout << COPLIEN_COLOR "RobotomyRequestForm constructor" RESET "\n";
	return;
}

RobotomyRequestForm::RobotomyRequestForm( RobotomyRequestForm const & src )
: AForm("robotomy_creation", src.getTarget(), 72, 45) {
	std::cout << COPLIEN_COLOR "RobotomyRequestForm copy constructor" RESET "\n";
	*this = src;
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

RobotomyRequestForm::~RobotomyRequestForm() {
	std::cout << COPLIEN_COLOR "RobotomyRequestForm destructor" RESET "\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

RobotomyRequestForm &	RobotomyRequestForm::operator=( RobotomyRequestForm const & rhs ) {
	AForm::operator=(rhs);
	return *this;
}

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	RobotomyRequestForm::formAction() const {
	std::cout << "*drill sounds*\n";
	if (std::rand() % 2)
		std::cout << _target << " robotomized with success\n";
	else
		std::cout << _target << " robotomization failed\n";
}

