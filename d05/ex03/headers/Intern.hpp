#ifndef INTERN_HPP
# define INTERN_HPP

# include "colors.h"
# include <iostream>
# include <string>
# include <algorithm> // trasnform
# include <cctype> // tolower

# include <AForm.hpp>
# include <ShrubberyCreationForm.hpp>
# include <PresidentialPardonForm.hpp>
# include <RobotomyRequestForm.hpp>

typedef AForm *	(*t_func)(std::string const & target);

typedef struct	s_formModel {
	std::string const	name;
	t_func const		create;
}				t_formModel;

class Intern {

public:
	Intern();
	Intern( Intern const & src );
	~Intern();
	Intern &	operator=( Intern const & rhs );

	AForm *	makeForm(std::string formName, std::string formTarget) const;

private:
	static const t_formModel	_chooseForm[];

	static AForm *	makeShrubbery(std::string const &target);
	static AForm *	makePresidential(std::string const &target);
	static AForm *	makeRobotomy(std::string const &target);

	bool			isValidForm(std::string name, unsigned int i) const;

};

#endif

