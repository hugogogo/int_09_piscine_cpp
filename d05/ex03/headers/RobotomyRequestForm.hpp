#ifndef ROBOTOMYREQUESTFORM_HPP
# define ROBOTOMYREQUESTFORM_HPP

# include "color.h"
# include <iostream>
# include <string>
# include <cstdlib>

# include "AForm.hpp"

class RobotomyRequestForm : public AForm {

public:

	RobotomyRequestForm( std::string target );
	RobotomyRequestForm( RobotomyRequestForm const & src );
	~RobotomyRequestForm();
	RobotomyRequestForm &	operator=( RobotomyRequestForm const & rhs );

	void formAction() const;

private:

	RobotomyRequestForm();
};

#endif

