#ifndef PRESIDENTIALPARDONFORM_HPP
# define PRESIDENTIALPARDONFORM_HPP

# include "color.h"
# include <iostream>
# include <string>
# include <cstdlib>

# include "AForm.hpp"

class PresidentialPardonForm : public AForm {

public:

	PresidentialPardonForm( std::string target );
	PresidentialPardonForm( PresidentialPardonForm const & src );
	~PresidentialPardonForm();
	PresidentialPardonForm &	operator=( PresidentialPardonForm const & rhs );

	void formAction() const;

private:

	PresidentialPardonForm();
};

#endif

