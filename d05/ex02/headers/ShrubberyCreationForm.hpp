#ifndef SHRUBBERYCREATIONFORM_HPP
# define SHRUBBERYCREATIONFORM_HPP

# include "color.h"
# include <iostream>
# include <string>
# include <fstream>

# include "AForm.hpp"

class ShrubberyCreationForm : public AForm {

public:

	ShrubberyCreationForm( std::string target );
	ShrubberyCreationForm( ShrubberyCreationForm const & src );
	~ShrubberyCreationForm();
	ShrubberyCreationForm &	operator=( ShrubberyCreationForm const & rhs );

	void formAction() const;

private:

	ShrubberyCreationForm();
};

#endif

