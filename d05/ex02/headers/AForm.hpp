#ifndef AFORM_HPP
# define AFORM_HPP

# include "color.h"
# include <iostream>
# include <string>

class Bureaucrat;
# include "Bureaucrat.hpp"

class AForm {

public:

	AForm( std::string name, std::string target, int signedGrade, int executeGrade );
	AForm( AForm const & src );
	virtual ~AForm() = 0;
	AForm &	operator=( AForm const & rhs );

	std::string	getName() const;
	std::string	getTarget() const;
	bool		getSigned() const;
	int			getSignedGrade() const;
	int			getExecuteGrade() const;

	void			beSigned( Bureaucrat const & b );
	void			execute(Bureaucrat const & executor) const;
	virtual void	formAction() const = 0;

private:

	AForm();

protected:

	class GradeTooHighException : public std::exception {
		const char * what() const throw();};
	class GradeTooLowException : public std::exception {
		const char * what() const throw();};
	class NotSignedException : public std::exception {
		const char * what() const throw();};

	std::string const	_name;
	std::string const	_target;
	bool				_signed;
	int const			_signedGrade;
	int const			_executeGrade;

};

std::ostream &	operator<<(std::ostream & o, AForm const & rhs);

#endif

