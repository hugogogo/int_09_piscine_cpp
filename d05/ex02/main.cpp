#include "Bureaucrat.hpp"
#include "AForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"

#define N_TEST "9"

int	main() {
	int i = 0;

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "Shrubbery ok :" RESET "\n";
	{
		Bureaucrat	b("natasha", 1);
		ShrubberyCreationForm s("sekoia");

		std::cout << s << '\n';
		std::cout << b << '\n';
			std::cout << B_BLUE "b.signForm :" RESET "\n";
		b.signForm(s);
		b.executeForm(s);
		std::cout << s << '\n';
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "Shrubbery too low sign and execute :" RESET "\n";
	{
		Bureaucrat	b("jordan", 150);
		ShrubberyCreationForm s("chemney");

		std::cout << s << '\n';
		std::cout << b << '\n';
			std::cout << B_BLUE "b.signForm :" RESET "\n";
		b.signForm(s);
		b.executeForm(s);
		std::cout << s << '\n';
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "Shrubbery too low execute only :" RESET "\n";
	{
		Bureaucrat	b("bernadette", 140);
		ShrubberyCreationForm s("rutabaga");

		std::cout << s << '\n';
		std::cout << b << '\n';
			std::cout << B_BLUE "b.signForm :" RESET "\n";
		b.signForm(s);
		b.executeForm(s);
		std::cout << s << '\n';
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "Bureaucrat copy test :" RESET "\n";
	{
		Bureaucrat	b1("pantoufle", 14);
		Bureaucrat	b2(b1);

		std::cout << b1 << "\n";
		std::cout << b2 << "\n";
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "Shrubbery copy test :" RESET "\n";
	{
		ShrubberyCreationForm s1("rutabaga");
		ShrubberyCreationForm s2(s1);

		std::cout << s1 << "\n";
		std::cout << s2 << "\n";
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "Presidential copy test :" RESET "\n";
	{
		PresidentialPardonForm p1("turnips");
		PresidentialPardonForm p2(p1);

		std::cout << p1 << "\n";
		std::cout << p2 << "\n";
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "Robotomy copy test :" RESET "\n";
	{
		RobotomyRequestForm r1("artichokes");
		RobotomyRequestForm r2(r1);

		std::cout << r1 << "\n";
		std::cout << r2 << "\n";
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "Robotomy :" RESET "\n";
	{
		Bureaucrat	b("romeo", 15);
		RobotomyRequestForm r("oven");

		std::cout << r << '\n';
		std::cout << b << '\n';
			std::cout << B_BLUE "b.signForm :" RESET "\n";
		b.signForm(r);
		b.executeForm(r);
		b.executeForm(r);
		b.executeForm(r);
		b.executeForm(r);
		b.executeForm(r);
		b.executeForm(r);
		b.executeForm(r);
		b.executeForm(r);
		b.executeForm(r);
		b.executeForm(r);
		b.executeForm(r);
		b.executeForm(r);
		b.executeForm(r);
		b.executeForm(r);
		std::cout << r << '\n';
	}

	std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
	<< "Presidential :" RESET "\n";
	{
		Bureaucrat	b("sylvestre", 1);
		PresidentialPardonForm p("queen");

		std::cout << p << '\n';
		std::cout << b << '\n';
			std::cout << B_BLUE "b.signForm :" RESET "\n";
		b.signForm(p);
		b.executeForm(p);
		std::cout << p << '\n';
	}

	std::cout << "\n";
	return 0;
}
