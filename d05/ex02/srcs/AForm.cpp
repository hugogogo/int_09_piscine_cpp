#include "AForm.hpp"
#define COPLIEN_COLOR B_CYAN

/*********************************************
 * CONSTRUCTORS
 *********************************************/

AForm::AForm( std::string name, std::string target, int signedGrade, int executeGrade )
: _name(name)
, _target(target)
, _signed(false)
, _signedGrade(signedGrade)
, _executeGrade(executeGrade) {
	if (signedGrade > 150 || executeGrade > 150)
		throw AForm::GradeTooLowException();
	if (signedGrade < 1 || executeGrade < 1)
		throw AForm::GradeTooHighException();
	std::cout << COPLIEN_COLOR "AForm constructor" RESET "\n";
	return;
}

AForm::AForm( AForm const & src )
: _name(src.getName())
, _signedGrade(src.getSignedGrade())
, _executeGrade(src.getExecuteGrade()) {
	std::cout << COPLIEN_COLOR "AForm copy constructor" RESET "\n";
	*this = src;
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

AForm::~AForm() {
	std::cout << COPLIEN_COLOR "AForm destructor" RESET "\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

AForm &	AForm::operator=( AForm const & rhs ) {
	if ( this != &rhs ) {
		_signed = rhs.getSigned();
	}
	return *this;
}

std::ostream &	operator<<(std::ostream & o, AForm const & rhs)
{
	o	<< "[form name]"
		<< rhs.getName() << ", [target]"
		<< rhs.getTarget() << ", [signed]"
		<< rhs.getSigned() << ", [sign grade]"
		<< rhs.getSignedGrade() << ", [exec grade]"
		<< rhs.getExecuteGrade();
	return (o);
}

/*********************************************
 * ACCESSORS
 *********************************************/

std::string	AForm::getName() const {return _name;}
std::string	AForm::getTarget() const {return _target;}
bool		AForm::getSigned() const {return _signed;}
int 		AForm::getSignedGrade() const {return _signedGrade;}
int 		AForm::getExecuteGrade() const {return _executeGrade;}

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	AForm::beSigned( Bureaucrat const & b) {
	if (b.getGrade() < _signedGrade)
		_signed = true;
	else
		throw AForm::GradeTooLowException();
}

void	AForm::execute(Bureaucrat const & executor) const {
	if (!_signed)
		throw NotSignedException();
	if (executor.getGrade() > _executeGrade)
		throw GradeTooLowException();
	formAction();
}

/*********************************************
 * NESTED CLASS
 *********************************************/

const char * AForm::GradeTooHighException::what() const throw() {
	return (B_RED "grade too high" RESET);
}
const char * AForm::GradeTooLowException::what() const throw() {
	return (B_RED "grade too low" RESET);
}
const char * AForm::NotSignedException::what() const throw() {
	return (B_RED "form is not signed" RESET);
}
