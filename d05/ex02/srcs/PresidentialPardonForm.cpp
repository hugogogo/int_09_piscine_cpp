#include "PresidentialPardonForm.hpp"

#define COPLIEN_COLOR B_CYAN

/*********************************************
 * CONSTRUCTORS
 *********************************************/

PresidentialPardonForm::PresidentialPardonForm( std::string target )
: AForm("presidential_creation", target, 25, 5){
	std::cout << COPLIEN_COLOR "RobotomyRequestForm constructor" RESET "\n";
	return;
}

PresidentialPardonForm::PresidentialPardonForm( PresidentialPardonForm const & src )
: AForm("presidential_creation", src.getTarget(), 25, 5) {
	std::cout << COPLIEN_COLOR "RobotomyRequestForm copy constructor" RESET "\n";
	*this = src;
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

PresidentialPardonForm::~PresidentialPardonForm() {
	std::cout << COPLIEN_COLOR "RobotomyRequestForm destructor" RESET "\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

PresidentialPardonForm &	PresidentialPardonForm::operator=( PresidentialPardonForm const & rhs ) {
	AForm::operator=(rhs);
	return *this;
}

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	PresidentialPardonForm::formAction() const {
	std::cout << "Zaphod Beeblebrox pardon " << _target << "\n";
}

