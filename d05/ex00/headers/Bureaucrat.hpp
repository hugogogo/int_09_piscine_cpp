#ifndef BUREAUCRAT_HPP
# define BUREAUCRAT_HPP

# include "color.h"
# include <iostream>
# include <string>
# include <stdexcept>

class Bureaucrat {

public:

	Bureaucrat(std::string name, int grade);
	Bureaucrat( Bureaucrat const & src );
	~Bureaucrat();
	Bureaucrat &	operator=( Bureaucrat const & rhs );

	std::string	getName() const;
	int			getGrade() const;

	void	gradeUp();
	void	gradeDown();

protected:

	std::string const	_name;
	int					_grade;

private:

	Bureaucrat();

	class GradeTooHighException : public std::exception {
		const char * what() const throw();};
	class GradeTooLowException : public std::exception {
		const char * what() const throw();};
};

std::ostream &	operator<<(std::ostream & o, Bureaucrat const & rhs);

#endif

