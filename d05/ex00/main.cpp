#include "Bureaucrat.hpp"
#define N_TEST "5"

int	main() {

	std::cout << B_YELLOW "\n[1/" N_TEST "] test too high :" RESET "\n";
	{
		try {
			Bureaucrat	b("clarence", 151);
		}
		catch (std::exception& e) {
			std::cout << e.what() << "\n";
		}
	}

	std::cout << B_YELLOW "\n[2/" N_TEST "] test too low :" RESET "\n";
	{
		try {
			Bureaucrat	b("ellen", 0);
		}
		catch (std::exception& e) {
			std::cout << e.what() << "\n";
		}
	}

	std::cout << B_YELLOW "\n[3/" N_TEST "] test change grade :" RESET "\n";
	{
		try {
			Bureaucrat	b("pacome", 51);
			std::cout << b << "\n";
			b.gradeDown();
			std::cout << b << "\n";
			b.gradeUp();
			std::cout << b << "\n";
		}
		catch (std::exception& e) {
			std::cout << e.what() << "\n";
		}
	}

	std::cout << B_YELLOW "\n[4/" N_TEST "] test reduce grade :" RESET "\n";
	{
		try
		{
			Bureaucrat	b("colin", 145);

			while (true)
			{
				b.gradeDown();
				std::cout << b << "\n";
			}
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << "\n";
		}
	}

	std::cout << B_YELLOW "\n[5/" N_TEST "] test increase grade :" RESET "\n";
	{
		try
		{
			Bureaucrat	b("colin", 5);

			while (true)
			{
				b.gradeUp();
				std::cout << b << "\n";
			}
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << "\n";
		}
	}

	std::cout << "\n";
	return 0;
}
