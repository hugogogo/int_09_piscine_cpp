#include "Bureaucrat.hpp"

#define COPLIEN_COLOR B_CYAN

/*********************************************
 * CONSTRUCTORS
 *********************************************/

Bureaucrat::Bureaucrat( std::string name, int grade )
: _name(name)
, _grade(grade) {
	if (grade > 150)
		throw GradeTooLowException();
	if (grade < 1)
		throw GradeTooHighException();
	std::cout << COPLIEN_COLOR "Bureaucrat constructor" RESET "\n";
	return;
}

Bureaucrat::Bureaucrat( Bureaucrat const & src ) {
	std::cout << COPLIEN_COLOR "Bureaucrat copy constructor" RESET "\n";
	*this = src;
	return;
}

/*********************************************
 * DESTRUCTORS
 *********************************************/

Bureaucrat::~Bureaucrat() {
	std::cout << COPLIEN_COLOR "Bureaucrat destructor" RESET "\n";
	return;
}

/*********************************************
 * OPERATORS
 *********************************************/

Bureaucrat &	Bureaucrat::operator=( Bureaucrat const & rhs ) {
	if ( this != &rhs )
		_grade = rhs.getGrade();
	return *this;
}

std::ostream &	operator<<(std::ostream & o, Bureaucrat const & rhs)
{
	o
		<< rhs.getName() << ", bureaucrat grade "
		<< rhs.getGrade();
	return (o);
}

/*********************************************
 * ACCESSORS
 *********************************************/

std::string	Bureaucrat::getName() const {return _name;}
int			Bureaucrat::getGrade() const {return _grade;}

/*********************************************
 * PUBLIC MEMBER FUNCTIONS
 *********************************************/

void	Bureaucrat::gradeUp() {
	if (_grade > 1)
		_grade--;
	else
		throw GradeTooHighException();
}

void	Bureaucrat::gradeDown() {
	if (_grade < 150)
		_grade++;
	else
		throw GradeTooLowException();
}

/*********************************************
 * NESTED CLASS
 *********************************************/

const char * Bureaucrat::GradeTooHighException::what() const throw() {
	return (B_RED "grade higher than 1" RESET);
}
const char * Bureaucrat::GradeTooLowException::what() const throw() {
	return (B_RED "grade lower than 150" RESET);
}
