#include <iostream>

/*
 * functions to print numbers in binary
 * for the float, found help from stackoverflow :
 * https://stackoverflow.com/questions/474007/floating-point-to-binary-valuec
 */

std::string printBitsInt(int num)
{
	int i = 0;

	for (unsigned int mask = 1U << (sizeof(int) *8 -1); mask; mask >>= 1)
	{
		std::cout << ((num & mask) != 0);
		i++;
		if (i == 1 || i == 9 || i == 24)
			std::cout << ' ';
	}
	return "";
}

std::string	printBitsFloat(float num)
{
	int	*p = (int *)&num;
	int i = 0;

	for (unsigned int mask = 1U << (sizeof(float) *8 -1); mask; mask >>= 1)
	{
		std::cout << ((*p & mask) != 0);
		i++;
		if (i == 1 || i == 9 || i == 24)
			std::cout << ' ';
	}
	return "";
}

