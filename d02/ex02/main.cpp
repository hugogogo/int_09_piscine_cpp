#include "Fixed.hpp"
#include <iostream>
#include <iomanip>
#include "color.h"

#define COLOR B_YELLOW
#define COLOR2 B_CYAN

int main( void ) {

	Fixed a;
	Fixed const b( Fixed( 5.05f ) * Fixed( 2 ) );

	std::cout << a << std::endl;
	std::cout << ++a << std::endl;
	std::cout << a << std::endl;
	std::cout << a++ << std::endl;
	std::cout << a << std::endl;

	std::cout << b << std::endl;

	std::cout << Fixed::max( a, b ) << std::endl;

	Fixed	c(10);
	Fixed	d(5);
	Fixed	e;

	std::cout << "c: " << c << '\n';
	std::cout << "d: " << d << "\n\n";

	// ">"
	std::cout << "\n" COLOR "operator >"" RESET " RESET "\n";
	std::cout << COLOR2 "e:" << e << " c:" << c << " d:" << d << RESET "\n";
	std::cout << "e = greater of (c, d) : e == " << (e = (c > d) ? c : d) << '\n';
	std::cout << "e > c ? : " << (e > c) << '\n';
	std::cout << "e > d ? : " << (e > d) << '\n';
	// "<"
	std::cout << "\n" COLOR "operator <" RESET "\n";
	std::cout << COLOR2 "e:" << e << " c:" << c << " d:" << d << RESET "\n";
	std::cout << "e = smaller of (c, d) : e == " << (e = (c < d) ? c : d) << '\n';
	std::cout << "e < c ? : " << (e < c) << '\n';
	std::cout << "e < d ? : " << (e < d) << '\n';
	// "<="
	std::cout << "\n" COLOR "operator <=" RESET "\n";
	std::cout << COLOR2 "e:" << e << " c:" << c << " d:" << d << RESET "\n";
	std::cout << "e <= c ? : " << (e <= c) << '\n';
	std::cout << "e <= d ? : " << (e <= d) << '\n';
	// ">="
	std::cout << "\n" COLOR "operator >=" RESET "\n";
	std::cout << COLOR2 "e:" << e << " c:" << c << " d:" << d << RESET "\n";
	std::cout << "e >= c ? : " << (e >= c) << '\n';
	std::cout << "e >= d ? : " << (e >= d) << '\n';
	// "=="
	std::cout << "\n" COLOR "operator ==" RESET "\n";
	std::cout << COLOR2 "e:" << e << " c:" << c << " d:" << d << RESET "\n";
	std::cout << "e == c ? : " << (e == c) << '\n';
	std::cout << "e == d ? : " << (e == d) << '\n';
	// "!="
	std::cout << "\n" COLOR "operator !=" RESET "\n";
	std::cout << COLOR2 "e:" << e << " c:" << c << " d:" << d << RESET "\n";
	std::cout << "e != c ? : " << (e != c) << '\n';
	std::cout << "e != d ? : " << (e != d) << '\n';
	// "+"
	std::cout << "\n" COLOR "operator +" RESET "\n";
	std::cout << COLOR2 "e:" << e << " c:" << c << " d:" << d << RESET "\n";
	std::cout << "e = c + d : " << (e = c + d) << "\n";
	std::cout << "e = e + c + d : " << (e = e + c + d) << '\n';
	// "-"
	std::cout << "\n" COLOR "operator -" RESET "\n";
	std::cout << COLOR2 "e:" << e << " c:" << c << " d:" << d << RESET "\n";
	std::cout << "e = d - e ? : " << (e = d - e) << "\n";
	std::cout << "e = c - d - e ? : " << (e = c - d - e) << '\n';
	// "*"
	Fixed	f;
	std::cout << "\n" COLOR "operator *" RESET "\n";
	std::cout << COLOR2 "e:" << e << " c:" << c << " d:" << d << RESET "\n";
	std::cout << "e = d * c ? : " << (e = d * c) << "\n";
	std::cout << "e = d * c * c ? : " << (e = d * c * c) << '\n';
	f = Fixed(3.5f);
	e = Fixed(5.25f);
	std::cout << COLOR2 "f:" << f << " e:" << e << " c:" << c << " d:" << d << RESET "\n";
	std::cout << "e = f * e ? : " << (e = f * e) << "\n";
	f = Fixed(1000);
	e = Fixed(50);
	std::cout << COLOR2 "f:" << f << " e:" << e << " c:" << c << " d:" << d << RESET "\n";
	std::cout << "e = f * e ? : " << (e = f * e) << "\n";
	f = Fixed(178481); // 8 388 607 / 47 = 178 481
	e = Fixed(47);
	std::cout << COLOR2 "f:" << f << " e:" << e << " c:" << c << " d:" << d << RESET "\n";
	std::cout << "e = f * e ? : " << (e = f * e) << "\n";
	// "/"
	std::cout << "\n" COLOR "operator /" RESET "\n";
	std::cout << COLOR2 "e:" << e << " c:" << c << " d:" << d << RESET "\n";
	std::cout << "e = d / c ? : " << (e = d / c) << "\n";
	std::cout << "e = c / d ? : " << (e = c / d) << "\n";
	std::cout << "e = d / c / e ? : " << (e = d / c / e) << '\n';
	c = Fixed(6);
	d = Fixed(10);
	std::cout << COLOR2 "e:" << e << " c:" << c << " d:" << d << RESET "\n";
	std::cout << "e = c / d ? : " << (e = c / d) << '\n';
	c = Fixed(6);
	d = Fixed(0);
	std::cout << COLOR2 "e:" << e << " c:" << c << " d:" << d << RESET "\n";
	std::cout << "e = c / d ? : " << (e = c / d) << '\n';
	// "+ - * /"
	std::cout << "\n" COLOR "operator + - * /" RESET "\n";
	Fixed	g;
	std::cout << COLOR2 "f:" << f << " e:" << e << " c:" << c << " d:" << d << RESET "\n";
	std::cout << "f = e + d / c - d / e * c ? : " << (f = e + d / c - d / e * c) << '\n';
	std::cout << "g = d / c ? : " << std::setw(5) << std::left << (g = d / c) << "[f:" << std::setw(5) << std::left << f << " g:" << std::setw(5) << std::left << g << " e:" << e << " c:" << c << " d:" << d << "]\n";
	std::cout << "g = e + g ? : " << std::setw(5) << std::left << (g = e + g) << "[f:" << std::setw(5) << std::left << f << " g:" << std::setw(5) << std::left << g << " e:" << e << " c:" << c << " d:" << d << "]\n";
	std::cout << "f = d / e ? : " << std::setw(5) << std::left << (f = d / e) << "[f:" << std::setw(5) << std::left << f << " g:" << std::setw(5) << std::left << g << " e:" << e << " c:" << c << " d:" << d << "]\n";
	std::cout << "f = f * c ? : " << std::setw(5) << std::left << (f = f * c) << "[f:" << std::setw(5) << std::left << f << " g:" << std::setw(5) << std::left << g << " e:" << e << " c:" << c << " d:" << d << "]\n";
	std::cout << "f = g - f ? : " << std::setw(5) << std::left << (f = g - f) << "[f:" << std::setw(5) << std::left << f << " g:" << std::setw(5) << std::left << g << " e:" << e << " c:" << c << " d:" << d << "]\n";
	// "++x"
	std::cout << "\n" COLOR "operator ++o" RESET "\n";
	e = Fixed(2);
	std::cout << COLOR2 "e:" << e << RESET "\n";
	std::cout << "++e : " << ++e << " ; e : " << e << '\n';
	// "--x"
	std::cout << "\n" COLOR "operator --o" RESET "\n";
	std::cout << COLOR2 "e:" << e << RESET "\n";
	std::cout << "--e : " << --e << " ; e : " << e << '\n';
	// "x++"
	std::cout << "\n" COLOR "operator o++" RESET "\n";
	std::cout << COLOR2 "e:" << e << RESET "\n";
	std::cout << "e++ : " << e++ << " ; e : " << e << '\n';
	// "x--"
	std::cout << "\n" COLOR "operator o--" RESET "\n";
	std::cout << COLOR2 "e:" << e << RESET "\n";
	std::cout << "e-- : " << e-- << " ; e : " << e << '\n';

	return 0;
}
