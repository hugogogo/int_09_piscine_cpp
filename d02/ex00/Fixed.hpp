#ifndef FIXED_HPP
# define FIXED_HPP

#include <iostream>
#include <string>

class Fixed {

public:

	 Fixed( void );								// default/parametric constructor
	 Fixed( Fixed const & src );				// copy constructor
	~Fixed( void );								// destructor

	Fixed &	operator=( Fixed const & rhs );	// assignement operator

	int		getRawBits( void ) const;
	void	setRawBits( int const raw );

private:

	int					_value;
	static int const	_frac = 8;

};

#endif

