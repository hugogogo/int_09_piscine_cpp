#include "Fixed.hpp"
#include <iostream>
#include "colors.h"

#define N_TEST "1"

int main( void ) {

    int i = 0;

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "tests subject :" RESET "\n";
	{
		Fixed a;
		Fixed b( a );
		Fixed c;
	
		c = b;
	
		std::cout << a.getRawBits() << std::endl;
		std::cout << b.getRawBits() << std::endl;
		std::cout << c.getRawBits() << std::endl;
	}

    std::cout << B_YELLOW "\n[" << ++i << "/" N_TEST "] "
    << "tests  sets :" RESET "\n";
	{
		Fixed a;
		Fixed b( a );
		Fixed c;
	
		c = b;
	
		std::cout << "\n" B_BLUE "sets a(4), b(6.3), c(-187)" RESET "\n";
		a.setRawBits(4);
		b.setRawBits(6.3);
		c.setRawBits(-187);
	
		std::cout << a.getRawBits() << std::endl;
		std::cout << b.getRawBits() << std::endl;
		std::cout << c.getRawBits() << std::endl;

		std::cout << "\n" B_BLUE "sets a(76.76), b(0), c(0.0)" RESET "\n";
		a.setRawBits(76.76);
		b.setRawBits(0);
		c.setRawBits(0.0);
	
		std::cout << a.getRawBits() << std::endl;
		std::cout << b.getRawBits() << std::endl;
		std::cout << c.getRawBits() << std::endl;
	}

	return 0;
}

// $> ./a.out
// Default constructor called
// Copy constructor called
// Copy assignment operator called // <-- This line may be missing depending on your implementation
// getRawBits member function called
// Default constructor called
// Copy assignment operator called
// getRawBits member function called
// getRawBits member function called
// 0
// getRawBits member function called
// 0
// getRawBits member function called
// 0
// Destructor called
// Destructor called
// Destructor called
// $>
